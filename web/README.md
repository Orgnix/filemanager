# Randal's FileManager

Randal's FileManager is a customizable, themeable file manager.

# Requirements
 - Composer
 - Git
 - Commandline

# Installation instructions

 - `git clone https://Orgnix@bitbucket.org/Orgnix/filemanager.git`
 - `composer install`
 - Edit default.settings.php to your liking (Do this BEFORE surfing to the URL!)
 - Surf to the URL, you should get redirected to ./install.php
 - Simply follow the instructions
 - -Remove install.php!-

Done!
Enjoy your new file manager.