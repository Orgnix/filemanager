<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
if (is_file(__DIR__ . '/settings.php')) {
  include_once(__DIR__ . '/settings.php');
}
include_once(__DIR__ . '/core/includes.php');

use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Core;
use FileManager\Settings;
use FileManager\Components\Filesystem;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use FileManager\Components\Theme;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

/**
 * If local only, show 403 page to everyone but the allowed remote ip
 */
if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

$Core = new Core();
if (!Core::isInstalled()) {
  header('Location: ./install.php');
  die;
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_session', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Session', ['user' => NULL, 'values' => [], 'log' => TRUE]])) {
  header('Location: ./login.php');
  die;
}

$Filesystem = new Filesystem();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
if (is_null($Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) || !$Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) {
  header('Location: ./login.php?logout');
}
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

$cached_settings = $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings');

$path = isset($_GET['path']) ? $cached_settings['root_folder'] . $_GET['path'] :  $cached_settings['root_folder'];
$uri_ready_folder = str_replace($cached_settings['root_folder'], '', $path);
$exploded_root = explode('/', $cached_settings['root_folder']);

if (!file_exists($path)) {
  if ($path !== $cached_settings['root_folder']) {
    header('Location: ./');
  } else {
    $status = 'ERROR: Specified root folder does not exist!';
  }
}

$Filesystem->updatePath($path);

$view_mode = $ViewMode->getFolderViewMode($path);
$all_view_modes = $Cache->getData('all_view_modes', '\\FileManager\\Components\\ViewMode', 'getAllViewModes');

$view_mode[0]['css'] = str_replace('{accentColorHover}', $Theme->getThemeInfo()->accentColors->accentColorHover, $view_mode[0]['css']);
$view_mode[0]['css'] = str_replace('{accentColorSelected}', $Theme->getThemeInfo()->accentColors->accentColorSelected, $view_mode[0]['css']);

if ($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['automatically_set_image_view_mode'] && $view_mode[0]['view_mode'] !== 'image') {
  $extensions = $Filesystem->getExtensionsInFolder($Filesystem->listFolderFiles($path));

  $images_only = TRUE;
  if (is_array($extensions) && count($extensions) > 0) {
    foreach ($extensions as $extension) {
      if (!in_array($extension, $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['file_ext']['image'])) {
        $images_only = FALSE;
        break;
      }
    }
    if ($images_only) {
      if (!$ViewMode->folderSettingExists($path)) {
        $ViewMode->setFolderViewMode($path, 'image');
        $view_mode = $ViewMode->getFolderViewMode($path);
      }
    }
  }
}

$fn_length = $view_mode[0]['max_fn_length'];
$fn_length_suffix = '...';
if ($fn_length == '0') {
  $fn_length = '999999999';
  $fn_length_suffix = '';
}

$files = [];

foreach ($Filesystem->listFolderFiles($path) as $file) {
  $Filesystem->addFileToDB($path, $file);
  $explode_file = explode('.', $file);
  $extension = array_pop($explode_file);
  $icon = file_exists('themes/' . USER_THEME . '/assets/img/filetypes/' . $extension . '.png') ? 'themes/' . USER_THEME . '/assets/img/filetypes/' . $extension . '.png' : 'themes/' . USER_THEME . '/assets/img/filetypes/file.png';
  $filename = strlen($file) > $fn_length ? substr($file, 0, $fn_length) . $fn_length_suffix : $file;
  $final_code = str_replace('{filename}', $filename, $view_mode[0]['code']);
  $final_code = str_replace('{full_filename}', $file, $final_code);
  $final_code = str_replace('{icon}', $icon, $final_code);
  $final_code = str_replace('{thumbnail}', $Filesystem->getThumbnailPath($path, $file), $final_code);
  $final_code = str_replace('{current_theme}', USER_THEME, $final_code);
  $final_code = str_replace('{filesize}', implode(' ', FileSystem::getFileSize($path . '/' . $file)) , $final_code);
  $final_code = str_replace('{creation_time}', FileSystem::getFileCreationTime($path . '/' . $file, TRUE), $final_code);
  $files[]['final_code'] = str_replace('{url}', $cached_settings['folder_as_uri'] . $uri_ready_folder . '/' . $file, $final_code);
}


$menus = [
  'folder' => $Cache->getData('folder_buttons', '\\FileManager\\Components\\Buttons', 'getAllButtons', ['folder']),
  'file' => $Cache->getData('file_buttons', '\\FileManager\\Components\\Buttons', 'getAllButtons', ['file']),
];

$template = $twig->load('index.html.twig');
echo $template->render([
  'debug' => $settings['debugging'],
  'view_mode' => $view_mode[0],
  'all_view_modes' => $all_view_modes,
  'assets' => $cached_settings['assets'],
  'menus' => $menus,
  'settings' => $cached_settings,
  'list_filesystem' => $Filesystem->listFilesystem(FALSE),
  'last_item_root' => array_pop($exploded_root),
  'path' => $path,
  'files' => $files,
  'uri_ready_folder' => $uri_ready_folder,
  'isset_path' => isset($_GET['path']),
  'permissions' => [
    'administer_site' => $Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']]),
  ],
  'status_bar' => [
    'status' => $status,
    'current_theme' => USER_THEME,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
