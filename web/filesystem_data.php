<?php

/**
 * Includes
 */
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;

$Filesystem = new Filesystem();

$mode = isset($_GET['mode']) ? $_GET['mode'] : 'json';
$path = isset($_GET['path']) ? $settings['root_folder'] . $_GET['path'] : $settings['root_folder'];

if ($path == $settings['root_folder'] . '/' || $path === $settings['root_folder']) {
  $folders = ['root' => $Filesystem->listFolderContent($path)];
} else {
  $folder = explode('/', $_GET['path']);
  $folders = [array_pop($folder) => $Filesystem->listFolderContent($path)];
}

if ($mode === 'json') {
  echo json_encode($folders);
} elseif ($mode === 'htmllist') {
  echo $Filesystem->HtmlListFiles($folders);
}
