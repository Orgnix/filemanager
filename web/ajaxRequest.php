<?php

namespace FileManager;

include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');
include_once(__DIR__ . '/../vendor/autoload.php');

use FileManager\Components\Assets;
use FileManager\Components\Cache;
use FileManager\Components\Filesystem;
use FileManager\Settings;
use Symfony\Component\HttpFoundation\File\File;
use UserFramework\Components\User;
use UserFramework\Components\Password;

$Settings = new Settings();

if (isset($_GET['mode'])) {
  if ($_GET['mode'] == 'getAssets') {
    $assets = new Assets($_GET['group'], $_GET['type']);
    echo $assets->getAssets();
  } elseif ($_GET['mode'] == 'getVMAssets') {
    $assets = new Assets();
    echo $assets->getViewModeAssets($_GET['viewmode'], $_GET['type']);
  } elseif ($_GET['mode'] == 'setViewMode') {
    $assets = new Assets(
      $_GET['table'],
      $_GET['field'],
      $_GET['value'],
      [$_GET['condition_field'] => $_GET['condition_value']]
    );
    $assets->setViewMode();
  } elseif ($_GET['mode'] == 'iplookup' && isset($_GET['ip'])) {
    $ip = $_GET['ip'];

    $ch = curl_init('http://api.ipapi.com/'.$ip.'?access_key=' . $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['ipapi_key']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $json = curl_exec($ch);
    curl_close($ch);

    $api_result = json_decode($json, true);

    echo 'ip: ' . $api_result['ip'] . '<br />';
    echo 'Country: <img src="' . $api_result['location']['country_flag'] . '" width="12px" height="12px" /> ' . $api_result['country_name'] . '<br />';
    echo 'Zip code: ' . $api_result['zip'] . '<br />';
    echo 'City: ' . $api_result['city'] . '<br />';
  } elseif($_GET['mode'] == 'updateTheme' && isset($_GET['theme'])) {
    $user = new User(User::getUsername());
    $user->setValue('theme', $_GET['theme']);
  } elseif ($_GET['mode'] == 'updateNewFolder' && isset($_GET['folder'])) {
    $cache = new Cache();
    $cache->updateNewPathCache($_GET['folder']);
  } elseif ($_GET['mode'] == 'renameFile') {
    $path = $_GET['path'];
    $old_fn = $_GET['old_filename'];
    $new_fn = $_GET['filename'];
    if (isset($path) && isset($old_fn) && isset($new_fn)) {
      if (is_file($path . '/' . $old_fn)) {
        rename($path . '/' . $old_fn, $path . '/' . $new_fn);
      }
    }
  } elseif ($_GET['mode'] == 'copyFiles') {
    $Filesystem = new Filesystem();
    $Filesystem->Copy();
  } elseif ($_GET['mode'] == 'moveFiles') {
    $Filesystem = new Filesystem();
    $Filesystem->Move();
  } elseif ($_GET['mode'] == 'deleteFiles') {
    $Filesystem = new Filesystem();
    $Filesystem->Delete();
  } elseif ($_GET['mode'] == 'compressFiles' && isset($_GET['compressed_file_name'])) {
    $Filesystem = new Filesystem();
    $Filesystem->CompressZip($_GET['compressed_file_name']);
  } elseif($_GET['mode'] == 'newFolder' && isset($_GET['path']) && isset($_GET['foldername'])) {
    mkdir($_GET['path'] . '/' . $_GET['foldername'], 0765);
  } elseif($_GET['mode'] == 'deleteFolder' && isset($_GET['path'])) {
    function rrmdir($dir) {
      if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
          if ($object != "." && $object != "..") {
            if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
          }
        }
        reset($objects);
        rmdir($dir);
      }
    }
    rrmdir($_GET['path']);
  } elseif($_GET['mode'] == 'renameFolder' && isset($_GET['path']) && isset($_GET['foldername'])) {
    $path = $_GET['path'];
    $path_exploded = explode('/', $path);
    end($path_exploded);
    $path_exploded[key($path_exploded)] = $_GET['foldername'];
    rename($path, implode('/', $path_exploded));
  } elseif($_GET['mode'] == 'fileUpload') {
    $filename = $_FILES['file']['name'];
    $filesize = $_FILES['file']['size'];
    $location = $_POST['path'] . '/' . $filename;

    $return_arr = array();

    $status = 'Could not upload..';
    if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
        $status = 'Succesfully uploaded!';
    }

    echo json_encode(['status' => $status]);
  } elseif($_GET['mode'] == 'changePassword') {
    $pw = new Password();
    $user = new User(User::getUsername());
    if ($pw->isPasswordSecure($_GET['password'])[0]) {
      if($user->setValue('password', $pw->securePassword($_GET['password'])[1])) {
        echo 'Password changed.';
      } else {
        echo 'Something went wrong and you will need to keep using your old password for now.';
      }
    } else {
      echo 'Password is not secure enough!';
    }
  }
} elseif (isset($_POST['mode'])) {
  if ($_POST['mode'] == 'saveFile') {
    $Filesystem = new Filesystem();
    $file = fopen($_POST['path'] . '/' . $_POST['filename'], 'w') or die('Unable to open file!');
    fwrite($file, $_POST['filecontent']);
    fclose($file);
    if ($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['create_revisions'] !== FALSE) {
      $Filesystem->fileRevision($_POST['path'] . '/' . $_POST['filename'], $_POST['filecontent']);
    }
  }
}
