function changeViewMode(path, view_mode) {
  $.get("./ajaxRequest.php?mode=setViewMode&table=folder_settings&field=view_mode&value=" + view_mode + "&condition_field=path&condition_value=" + path ,
  function(data) {
    location.reload();
  });
}

function changeTheme(theme) {
  $.get("./ajaxRequest.php?mode=updateTheme&theme=" + theme ,
  function(data) {
    location.reload();
  });
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

$(document).ready(function() {
  $(function() {
    $("#dialog").dialog({
      close: function( event, ui ) {
        $('.body_cover').css('display', 'none');
      },
      open: function( event, ui ) {
        $('.body_cover').css('display', 'block');
      },
      autoOpen: false,
      title: "Dialog",
      width: 500,
      height: 320
    });
  });
  
  $(".changePassword").on("click", function() {
    $("#dialog").dialog("option", "title", "Change password");
    $("#dialog").dialog("option", "height", 320);
    $(".dialog__content").load("./actions/user/change_password.php", function() {
      $("#dialog").dialog("open");
    });
  });

  $(".File").click(function() {
    $(this).toggleClass("selected");
    let content = $(this).find('.FileName').attr('title');
    let arrayIndex = action_array.indexOf(content);
    if (arrayIndex === -1) {
      action_array.push(content);
    } else {
      action_array.splice(arrayIndex, 1);
    }
    jsonString = JSON.stringify({ ...action_array });

    allFiles = replaceAll(jsonString, ' ', '%20');
    
    if(action_array.length === 0) {
      $(".filebtn").css("display", "none");
      $(".button_delete").css("display", "none");
      $(".button_rename").css("display", "none");
      $(".button_edit").css("display", "none");
      $(".button_copy").css("display", "none");
      $(".button_move").css("display", "none");
      $(".button_compress").css("display", "none");
    } else if(action_array.length === 1) {
      $(".filebtn").css("display", "block").html("File");
      $(".button_delete").css("display", "block");
      $(".button_rename").css("display", "block");
      $(".button_edit").css("display", "block");
      $(".button_copy").css("display", "block");
      $(".button_move").css("display", "block");
      $(".button_compress").css("display", "block");
    } else {
      $(".filebtn").css("display", "block").html("Files");
      $(".button_delete").css("display", "block");
      $(".button_rename").css("display", "none");
      $(".button_edit").css("display", "none");
      $(".button_copy").css("display", "block");
      $(".button_move").css("display", "block");
      $(".button_compress").css("display", "block");
    }
  });
});
