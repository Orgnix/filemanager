<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Core;
use FileManager\Settings;
use FileManager\Components\Database;
use FileManager\Components\Filesystem;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use FileManager\Components\Url;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

/**
 * If local only, show 403 page to everyone but the allowed remote ip
 */
if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_session', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Session', ['user' => NULL]])) {
  header('Location: ./login.php');
}

$Core = new Core();
$Url = new Url();
$Filesystem = new Filesystem();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
if (is_null($Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) || !$Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) {
  header('Location: ./login.php?logout');
}
$loader = new FilesystemLoader('themes/custom');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);
$Database = new Database('AND', 'movies');

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

$sort_key = isset($_GET['sort_key']) ? $_GET['sort_key'] : 'movie';
$sort = isset($_GET['sort']) ? $_GET['sort'] : 'ASC';

$opposite_sort = $sort == 'ASC' ? 'DESC' : 'ASC';

$links = [
  'sort_key_watched' => $Url->addParamsToCurrentUrl(['sort_key' => 'watched', 'sort' => $opposite_sort]),
  'sort_key_movie' => $Url->addParamsToCurrentUrl(['sort_key' => 'movie', 'sort' => $opposite_sort]),
  'sort_key_added' => $Url->addParamsToCurrentUrl(['sort_key' => 'added', 'sort' => $opposite_sort]),
  'sort_key_updated' => $Url->addParamsToCurrentUrl(['sort_key' => 'updated', 'sort' => $opposite_sort]),
];

foreach ($_GET as $key => $value) {
  $links['remove_param'][$key] = $Url->removeParamsFromCurrentUrl($key);
}

if (isset($_GET['update'])) {
  $watched = 0;
  if ($_GET['status'] == '0') {
    $watched = 1;
  }

  $query = $Database->update('list')
                    ->values([
                      'watched' => $watched,
                      'updated' => time(),
                    ])
                    ->condition('id', $_GET['id']);

  if ($query->execute()) {
    header('Location: ./movies.php');
  } else {
    echo 'Something went wrong trying to update this movie!';
  }
}

$existing_movies = [];
// Update database
if (is_dir("/srv/dev-disk-by-label-Main/Movies/")) {
  if ($dir_handler = opendir("/srv/dev-disk-by-label-Main/Movies/")) {
    while (($file = readdir($dir_handler)) !== false) {
      if ($file !== "." && $file !== "..") {
        $existing_movies[] = $file;
        $sel_movies = $Database->select('list')
                               ->fields(NULL, ['id'])
                               ->condition('movie', $file);
        if (!$sel_movies->execute()) {
          //break;
        }

        $results = $sel_movies->fetchAllAssoc();

        if (count($results) == 0) {
          $insert_movie = $Database->insert('list')
                                   ->values([
                                     '',
                                     $file,
                                     0,
                                     time(),
                                     time(),
                                   ]);
          if (!$insert_movie->execute()) {
            echo 'Something went wrong trying to add this movie to the database';
          }
        }
      }
    }
    closedir($dir_handler);
  }
}

asort($existing_movies);

$movies = $Database->select('list')
                   ->orderBy($sort_key, $sort);
if ($sort_key !== 'movie') {
  $movies->orderBy('movie', 'ASC');
}
if (!$movies->execute()) {
  echo 'Something went wrong trying to fetch the movies!';
  exit;
}
$movies = $movies->fetchAllAssoc('movie');

foreach ($movies as $key => $movie) {
  if (file_exists("/srv/dev-disk-by-label-Main/WebServer/Files/movies/" . $movie['movie'])) {
    $movies[$key]['fm'] = TRUE;
  } else {
    if(isset($_GET['move']) && $movie['id'] == $_GET['move']) {
      Filesystem::copyDir("/srv/dev-disk-by-label-Main/Movies/" . $movie['movie'], "/srv/dev-disk-by-label-Main/WebServer/Files/movies/" . $movie['movie']);
      exit;
    }
  }
  if (!in_array($key, $existing_movies)) {
    unset($movies[$key]);
  }
}

$template = $twig->load('movies.html.twig');
echo $template->render([
  'debug' => $settings['debugging'],
  'assets' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['assets'],
  'settings' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings'),
  'movies' => $movies,
  'existing_movies' => $existing_movies,
  'links' => $links,
  'permissions' => [
    'administer_site' => $Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']]),
  ],
  'status_bar' => [
    'status' => $status,
    'current_theme' => USER_THEME,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);