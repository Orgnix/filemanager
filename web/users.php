<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;
use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Settings;
use FileManager\CreateUser;
use FileManager\Core;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']])) {
  header('Location: ./login.php');
  die;
}

$Core = new Core();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

if (isset($_GET['user'])) {
  $selected_user = $User->getUser($_GET['user']);
}

if (isset($_POST['user_save'])) {
  $values = [
    'username' => $_POST['user_username'],
    'email' => $_POST['user_email'],
    'permissions' => json_encode($_POST['user_permissions']),
    'theme' => $_POST['user_theme'],
  ];

  $User->saveUserValues($_POST['user_id'], $values);
  header('Location: ?user=' . $_POST['user_id'] . '&message=' . json_encode(['User ' . $_POST['username'] . ' was saved.']));
} elseif (isset($_POST['user_add'])) {
  $new_user = new CreateUser($_POST['user_username'], 'FileManager_987', $_POST['user_email']);
  $Core->log('Created or attempted to create user <i>' . $_POST['user_username'] . '</i>');
  header('Location: ?message=' . json_encode($new_user->return()[1]));
}

if (isset($_GET['remove']) && $_GET['remove'] == 'consent') {
  $Core->log('Removed user <i>' . $selected_user['username'] . '</i>');
  $User->remove($selected_user['unique_id']);
  header('Location: users.php');
}

$all_users = $User->getAllUsers();
foreach ($all_users as $key => $user) {
  if ($user['unique_id'] == 0) {
    unset($all_users[$key]);
    break;
  }
}

$template = $twig->load('users.html.twig');
echo $template->render([
  'debug' => $Settings->getSetting('debugging'),
  'selected_user' => isset($selected_user) ? $selected_user : NULL,
  'users' => $all_users,
  'assets' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['assets'],
  'settings' => $settings,
  'message' => isset($_GET['message']) ? json_decode($_GET['message']) : NULL,
  'remove' => isset($_GET['remove']),
  'status_bar' => [
    'status' => $status,
    'current_theme' => $Theme->getThemeInfo(USER_THEME)->label,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
