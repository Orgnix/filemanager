<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/default.settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use FileManager\Core;
use FileManager\Components\Database;

$loader = new FilesystemLoader('themes/dark/');
$twig = new Environment($loader);

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

$core = new Core();
if (Core::isInstalled()) {
  header('Location: ./index.php');
}
if (isset($_POST['install_submit'])) {
  $db = new Database();
  $query = '';
  $sqlScript = file('core/empty.sql');
  foreach ($sqlScript as $line) {

    $startWith = substr(trim($line), 0, 2);
    $endWith = substr(trim($line), -1, 1);

    if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
      continue;
    }

    $query = $query . $line;
    if ($endWith == ';') {
      $query = str_replace('{USERNAME}', $_POST['install_username'], $query);
      $query = str_replace('{PASSWORD}', password_hash($_POST['install_password'], PASSWORD_DEFAULT), $query);
      $db->query($query);
      $query = '';
    }
  }
  echo '<div class="success-response sql-import-response">SQL file imported successfully</div>';
  copy(__DIR__ . '/default.settings.php', __DIR__ . '/settings.php');
}


$template = $twig->load('install.html.twig');
echo $template->render([
  'title' => 'Install Filemanager',
  'debug' => $settings['debugging'],
  'status_bar' => [
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
