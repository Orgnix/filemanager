<?php

header('Refresh:1;url=./');

/**
 * Includes
 */
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');
include_once(__DIR__ . '/../vendor/autoload.php');

use UserFramework\Components\Permissions;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Filemanager\Login;
use UserFramework\Components\User;

if (isset($_SESSION['username'])) {
  $User = new User($_SESSION['username']);
  $loader = new FilesystemLoader('themes/' . $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme'] . '/');
} else {
  $loader = new FilesystemLoader('themes/dark/');
}
$twig = new Environment($loader);

$template = $twig->load('after_login.html.twig');


echo $template->render([
  'message' => 'HI MYSTARD',
]);
