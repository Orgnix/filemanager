<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;
use FileManager\Settings;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);
$Settings = new Settings();

$path = $_GET['path'];

$files = [];
$files_temp = json_decode($_GET['files']);
foreach ($files_temp as $file) {
  $files[] = ['name' => $file];
}

// Update the files cache in the database
$Action->updateFilesCache($files);

$filename = $files[0]['name'];

$current_tree = explode('/', $path);
$current_folder = array_pop($current_tree);

$root_tree = explode('/', $settings['root_folder']);
$root_folder = array_pop($root_tree);

$filesystem = $Filesystem->StackOptions($Action->getAllFolders(), 1, '↪', '/' . $current_folder);
$file_path = $path . '/' . $filename;

if (in_array(Filesystem::getFileExtension($filename), $settings['file_ext']['editable'])) {
  $content = file_get_contents($file_path);
}

echo $Action->setActionName('Edit')
            ->setTemplateFolder('files')
            ->loadTwigTemplate()
            ->setVariables([
              'title' => 'Edit files',
              'button_text' => 'Save',
              'path' => $path,
              'filename' => $filename,
              'content' => $content ?? 'File does not exist or is not an editable file.',
              'ajax_link' => './ajaxRequest.php',
            ])
            ->render();
