<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);

$path = $_GET['path'];

$files = [];
$files_temp = json_decode($_GET['files']);
foreach ($files_temp as $file) {
  $files[] = ['name' => $file];
}

// Update the files cache in the database
$Action->updateFilesCache($files);

$current_tree = explode('/', $path);
$current_folder = array_pop($current_tree);

$root_tree = explode('/', $settings['root_folder']);
$root_folder = array_pop($root_tree);

echo $Action->setActionName('Compress')
            ->setTemplateFolder('files')
      			->loadTwigTemplate()
      			->setVariables([
      			  'title' => 'Compress files into archive',
      			  'button_text' => 'Compress',
      			  'files' => $files,
              'ajax_link' => './ajaxRequest.php',
      			])
      			->render();