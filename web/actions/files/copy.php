<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);

$path = $_GET['path'];

$files = [];
$files_temp = json_decode($_GET['files']);
foreach ($files_temp as $file) {
  $files[] = ['name' => $file];
}

// Update the files cache in the database
$Action->updateFilesCache($files);

$current_tree = explode('/', $path);
$current_folder = array_pop($current_tree);

$root_tree = explode('/', $settings['root_folder']);
$root_folder = array_pop($root_tree);

$filesystem = $Filesystem->StackOptions($Action->getAllFolders(), 1, '↪', '/' . $current_folder);

echo $Action->setActionName('Copy')
						->setTemplateFolder('files')
						->loadTwigTemplate()
						->setVariables([
						  'title' => 'Copy files',
						  'button_text' => 'Copy',
						  'files' => $files,
						  'files_json' => $_GET['files'],
						  'filesystem' => $filesystem,
						  'filesystem_data' => $Action->getAllFolders(),
						  'root_folder' => $root_folder,
              'current_folder' => $current_folder,
              'ajax_link' => './ajaxRequest.php',
						])
						->render();
