<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;
use FileManager\Settings;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);
$Settings = new Settings();

echo $Action->setActionName('Change Password')
            ->setTemplateFolder('user')
            ->loadTwigTemplate()
            ->setVariables([
              'title' => 'Change password',
              'button_text' => 'Save',
              'ajax_link' => './ajaxRequest.php',
            ])
            ->render();
