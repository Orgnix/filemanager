<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;
use FileManager\Settings;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);
$Settings = new Settings();

$path = $_GET['path'];

echo $Action->setActionName('Upload')
            ->setTemplateFolder('folders')
            ->loadTwigTemplate()
            ->setVariables([
              'title' => 'Upload files to folder',
              'button_text' => 'Upload',
              'root_folder' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'],
              'path' => $path,
              'ajax_link' => './ajaxRequest.php',
            ])
            ->render();
