<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;
use FileManager\Settings;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);
$Settings = new Settings();

$path = $_GET['path'];
$path_exploded = explode('/', $path);
$foldername = array_pop($path_exploded);

echo $Action->setActionName('Rename')
            ->setTemplateFolder('folders')
            ->loadTwigTemplate()
            ->setVariables([
              'title' => 'Rename folder',
              'button_text' => 'Rename',
              'root_folder' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'],
              'path' => $path,
              'folder' => $foldername,
              'ajax_link' => './ajaxRequest.php',
            ])
            ->render();
