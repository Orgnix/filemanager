<?php

include('../../settings.php');
include_once('../../core/includes.php');
include_once('../../../vendor/autoload.php');

use UserFramework\Components\User;
use FileManager\Components\Actions;
use FileManager\Components\Filesystem;
use FileManager\Settings;

$User = new User(User::getUsername());
$Filesystem = new Filesystem();
$Action = new Actions($User);
$Settings = new Settings();

$path = $_GET['path'] . '/';

$path_without_root = str_replace($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'], '', $path);

echo $Action->setActionName('Delete')
            ->setTemplateFolder('folders')
      			->loadTwigTemplate()
      			->setVariables([
      			  'title' => 'Delete folder',
      			  'button_text' => 'Delete',
              'path' => $path,
              'ajax_link' => './ajaxRequest.php',
      			])
      			->render();
