<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Core;
use FileManager\Settings;
use FileManager\Components\Filesystem;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use FileManager\Components\Url;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

/**
 * If local only, show 403 page to everyone but the allowed remote ip
 */
if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_session', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Session', ['user' => NULL]])) {
  header('Location: ./login.php');
  die;
}

$Core = new Core();
$Url = new Url();
$Filesystem = new Filesystem();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
if (is_null($Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) || !$Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']) {
  header('Location: ./login.php?logout');
}
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

if (isset($_GET['remove'])) {
  $Core->removeLogs();
  header('Location:logs.php');
}

$limit = isset($_GET['limit']) ? $_GET['limit'] : 50;
$sort_key = isset($_GET['sort_key']) ? $_GET['sort_key'] : 'id';
$sort = isset($_GET['sort']) ? $_GET['sort'] : 'DESC';

$opposite_sort = $sort == 'ASC' ? 'DESC' : 'ASC';

if ($limit == 0) {
  $limit = FALSE;
}

$links = [
  'sort_key_id' => $Url->addParamsToCurrentUrl(['sort_key' => 'id', 'sort' => $opposite_sort]),
  'sort_key_username' => $Url->addParamsToCurrentUrl(['sort_key' => 'username', 'sort' => $opposite_sort]),
  'sort_key_ip' => $Url->addParamsToCurrentUrl(['sort_key' => 'ip', 'sort' => $opposite_sort]),
  'sort_key_log' => $Url->addParamsToCurrentUrl(['sort_key' => 'log', 'sort' => $opposite_sort]),
  'sort_key_path' => $Url->addParamsToCurrentUrl(['sort_key' => 'path', 'sort' => $opposite_sort]),
  'sort_key_timestamp' => $Url->addParamsToCurrentUrl(['sort_key' => 'timestamp', 'sort' => $opposite_sort]),
  'sort' => $Url->addParamsToCurrentUrl('sort', $opposite_sort),
  'current_url' => $Url->getUrl(),
  'url_without_params' => $Url->getUrlWithoutParameters(),
  'url' => $Url,
];

foreach ($_GET as $key => $value) {
  $links['remove_param'][$key] = $Url->removeParamsFromCurrentUrl($key);
}

$template = $twig->load('logs.html.twig');
echo $template->render([
  'debug' => $settings['debugging'],
  'logs' => $Core->getLogs($limit, $sort_key, $sort, $_GET['search_term'] ?? NULL),
  'assets' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['assets'],
  'settings' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings'),
  'links' => $links,
  'filters' => $_GET,
  'status_bar' => [
    'status' => $status,
    'current_theme' => USER_THEME,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
