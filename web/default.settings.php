<?php

namespace FileManager;

if (!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
  header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
  exit;
}

session_start();
ob_start();

// Change this to your country's capital city: https://www.php.net/manual/en/timezones.php
date_default_timezone_set('Europe/Brussels');

$settings = [];

$settings['debugging'] = FALSE;
$settings['local_only'] = FALSE;
$settings['local_only_remote_addr'] = [
  '192.168.0.1',
  '127.0.0.1',
];

/** Max time in seconds before page will time out */
$settings['max_execution_time'] = 600;

/**
 * Database info
 */
$settings['database']['hostname'] = 'db host';
$settings['database']['username'] = 'db username';
$settings['database']['password'] = 'db password';
$settings['database']['database'] = 'db name';

/**
 * Folder info
 */
// Project root (No trailing slash!)
$settings['project_root'] = '/path/to/this/script';

// Complete folder structure (No trailing slash!)
$settings['root_folder'] = '/path/to/file/system';

// The base for the download link (should link to the project root) (No trailing slash!)
$settings['root_as_uri'] = 'https://www.yourwebsite.com';

// The base for the download link (should link to the folder above) (No trailing slash!)
$settings['folder_as_uri'] = 'https://files.yourwebsite.com/';

// Folders to be excluded (without root path, eg: /parent/child/child_of_child )
$settings['folders']['excluded_folders'] = [
  '',
];

/**
 * API's
 */
// The key for the IP api (in the logs) : https://ipapi.com/
$settings['ipapi_key'] = '8d161051aaeb561accd0d1bb927fd0fa';

/**
 * Personalization
 */
// TRUE or FALSE (automatically sets folder view mode to 'image' if it detects all files in it are images)
$settings['automatically_set_image_view_mode'] = TRUE;

/**
 * Styling options
 */
// The below code applies to folders in the left panel,
// The selected item will be surrounded by the html tags below.
$settings['selected_item_prefix'] = '<strong style="text-shadow: 0 1px 4px #21ba1b;"><i>';
$settings['selected_item_suffix'] = '</i></strong>';

/**
 * File settings
 */
// Maximum size an image should have (in bytes) before a thumbnail is created
$settings['image_size_limit'] = 1024000; // Standard 1024000 bytes = 1 MB
// After how long the folder will index image files and create thumbnails
$settings['index_frequency'] = 10800; // Standard 10800 seconds = 3 hours
// Thumbnails are made in the same folder as the original image, but with a prefix
// They are NOT listed in the folder when viewing the web-based file manager
$settings['thumbnail_prefix'] = '.thumbnail_'; // Adding a dot at the start makes it a hidden file
// Create revisions of files when you save them (saves old content to database)
$settings['create_revisions'] = TRUE;

/**
 * File extensions
 */
$settings['file_ext'] = [];
$settings['file_ext']['image'] = ['png', 'jpg', 'jpeg', 'gif', 'svg', 'bmp'];
$settings['file_ext']['video'] = ['mov', 'mp4', 'avi', 'mpeg', 'mkv', 'flv', 'webm', 'wmv', 'm4v'];
$settings['file_ext']['code'] =  ['php', 'html', 'sql', 'js', 'xml', 'asp', 'vb', 'sh', 'css', 'lua', 'py', 'rb'];
$settings['file_ext']['text'] =  ['txt', 'odt', 'rtf', 'doc', 'docx', 'log'];

$settings['file_ext']['editable'] = ['txt', 'log', 'php', 'html', 'js', 'xml', 'asp', 'vb', 'sh', 'sql', 'css', 'lua', 'py', 'rb'];

/**
 * Asset links
 */
$settings['assets']['js'] = [
  './ajaxRequest.php?mode=getAssets&group=file_buttons&type=js',
  './ajaxRequest.php?mode=getAssets&group=folder_buttons&type=js',
];
$settings['assets']['css'] = [
  './ajaxRequest.php?mode=getAssets&group=file_buttons&type=css',
  './ajaxRequest.php?mode=getAssets&group=folder_buttons&type=css',
];

/**
 * Unique token
 */
$settings['token'] = 'x3e)918E(dokC3#*!(eE3@11)@1Xpo*oiD#';

////////////////////////////////////////////////////////////////////////////
/////////////         NO CHANGES BELOW THIS LINE         ///////////////////
////////////////////////////////////////////////////////////////////////////

/**
 * All permissions (used when creating/adjusting users)
 */
$settings['permissions'] = [
  'Administer site',
  'Add content',
  'Edit content',
  'Delete content',
];

// These settings are too sensitive or irrelevant to import
$settings['excluded'] = [
  'debugging',
  'local_only',
  'local_only_remote_addr',
  'database',
  'permissions',
  'excluded',
  'description',
  'human_name',
];

// Explains each setting (displayed on the options page)
$settings['description']['max_execution_time'] = 'The maximum time that should pass before the page times out (Important to set high when compressing large files)';
$settings['description']['create_revisions'] = 'Creates revision for a file (only once saved) and writes it to the database';
$settings['description']['project_root'] = 'Root folder for this code';
$settings['description']['root_folder'] = 'Root folder where the file manager should begin reading';
$settings['description']['root_as_uri'] = 'The root uri for this code';
$settings['description']['folder_as_uri'] = 'The root uri for the files (used to make a DL link)';
$settings['description']['automatically_set_image_view_mode'] = 'Sets view mode to image if only images are detected';
$settings['description']['selected_item_prefix'] = 'In the folder picker, sets the HTML prefix for the selected item';
$settings['description']['selected_item_suffix'] = 'In the folder picker, sets the HTML suffix for the selected item';
$settings['description']['image_size_limit'] = 'Image size limit before thumbnails are created in bytes';
$settings['description']['index_frequency'] = 'Amount of time in seconds that should pass between indexing folders';
$settings['description']['thumbnail_prefix'] = 'The prefix a thumbnail should get (Best practice: start with a dot so it\'s hidden in the actual filesystem)';
$settings['description']['file_ext'] = 'File extensions per file type';
$settings['description']['assets'] = 'Links for all assets (js, css, ..)';
$settings['description']['token'] = 'The unique token a user has to have to be able to use the site. (Don\'t enter commas)';

ini_set('max_execution_time', $settings['max_execution_time']);
