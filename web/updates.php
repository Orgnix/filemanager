<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;
use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Settings;
use FileManager\Core;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']])) {
  header('Location: ./login.php');
  die;
}

$Core = new Core();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

$getUpdates = $Core->getUpdates();

$updates = [
  'core' => $getUpdates->core,
  'themes' => $getUpdates->themes,
];
$versions['core'] = Core::FILEMANAGER_VERSION;
foreach ($Theme->getThemes() as $theme => $parameters) {
  $versions['themes'][$theme] = $parameters->version;
}

// Turn stdObject into array.
$updates = json_decode(json_encode($updates), True);

if (isset($_GET['update'])) {
  $exploded = explode('/', $_GET['update']);
  if (count($exploded) == 2) {
    $selected_update[$exploded[1]] = ['type' => $exploded[0]] + $updates[$exploded[0]][$exploded[1]];
  } elseif(count($exploded) == 1) {
    $selected_update[$exploded[0]] = $updates[$exploded[0]];
  }
}

if (isset($_GET['doUpdate'])) {
  $execOutput = $Core->update();
} elseif (isset($_GET['updateInfo'])) {
  $execOutput = $Core->updateInfo();
}

$template = $twig->load('updates.html.twig');
echo $template->render([
  'debug' => $settings['debugging'],
  'versions' => $versions,
  'updates' => $updates,
  'selected_update' => $selected_update ?? NULL,
  'doUpdate' => isset($_GET['doUpdate']),
  'updateInfo' => isset($_GET['updateInfo']),
  'execOutput' => $execOutput ?? NULL,
  'assets' => $settings['assets'],
  'settings' => $settings,
  'status_bar' => [
    'status' => $status,
    'current_theme' => $Theme->getThemeInfo(USER_THEME)->label,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
