<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;
use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Settings;
use FileManager\Core;
use FileManager\Components\Buttons;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']])) {
  header('Location: ./login.php');
  die;
}

$Core = new Core();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$Settings->importSettings(isset($_GET['ImportSettings']) && $_GET['ImportSettings'] == 'consent');
$status = $Core->cron();
/* STOP CRON */

$template = $twig->load('options.html.twig');

$original_settings = $settings;
foreach ($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings') as $setting_key => $setting_value) {
  if (isset($original_settings[$setting_key])) {
    if ($setting_value === $original_settings[$setting_key]) {
      unset($original_settings[$setting_key]);
    }
  }
}
foreach ($settings['excluded'] as $key) {
  if (isset($original_settings[$key])) {
    unset($original_settings[$key]);
  }
}
$new_settings = [];
foreach ($original_settings as $setting_key => $setting_value) {
  if (!isset($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')[$setting_key])) {
    $new_settings[$setting_key] = $setting_value;
  }
}

if (isset($_GET['edit']) && isset($_POST['setting_save'])) {
  $saved_settings = $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings');
  if (isset($_POST['setting_iterable'])) {
    // @TODO : make this.
    foreach ($Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')[$_GET['edit']] as $key=>$value) {
      if (!isset($_POST['setting_' . $_GET['edit'] . '_' . $key])) {
        $_POST['setting_' . $_GET['edit'] . '_' . $key] = 0;
      }
      if (is_numeric($_POST['setting_' . $_GET['edit'] . '_' . $key])) {
        $_POST['setting_' . $_GET['edit'] . '_' . $key] = (int)$_POST['setting_' . $_GET['edit'] . '_' . $key];
      }
      $saved_settings[$_GET['edit']][$key] = explode(',', $_POST['setting_' . $_GET['edit'] . '_' . $key]);
    }
    $Settings->importSettings(TRUE, $saved_settings);
    header('Refresh:0;');
  } else {
    if (!isset($_POST['setting_' . $_GET['edit']])) {
      $_POST['setting_' . $_GET['edit']] = 0;
    }
    if (is_numeric($_POST['setting_' . $_GET['edit']])) {
      $_POST['setting_' . $_GET['edit']] = (int)$_POST['setting_' . $_GET['edit']];
    }
    $saved_settings[$_GET['edit']] = $_POST['setting_' . $_GET['edit']];
    $Settings->importSettings(TRUE, $saved_settings);
    header('Refresh:0;');
  }
}

echo $template->render([
  'debug' => $settings['debugging'],
  'assets' => $settings['assets'],
  'settings' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings'),
  'new_settings' => $new_settings,
  'original_settings' => $original_settings,
  'descriptions' => $settings['description'],
  'edit' => isset($_GET['edit']) ? ['key' => $_GET['edit'], 'values' => $Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')[$_GET['edit']]] : FALSE,
  'importsettings' => isset($_GET['ImportSettings']),
  'imported_settings' => isset($_GET['ImportSettings']) && $_GET['ImportSettings'] == 'consent',
  'status_bar' => [
    'status' => $status,
    'current_theme' => USER_THEME,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
