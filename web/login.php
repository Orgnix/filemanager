<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use UserFramework\Components\Permissions;
use UserFramework\Components\User;
use Twig\Loader\ArrayLoader;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use FileManager\Login;
use FileManager\Core;

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

if (isset($_SESSION['username'])) {
  $User = new User($_SESSION['username']);
  $loader = new FilesystemLoader('themes/' . $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme'] . '/');
} else {
  $loader = new FilesystemLoader('themes/dark/');
}
$twig = new Environment($loader);
$core = new Core();

if (isset($_GET['logout'])) {
  $core->log('Session closed for ' . $_SESSION['username']);
  unset($_SESSION['username']);
  unset($_SESSION['password']);
  unset($_SESSION['token']);
  // unset cookies
  if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
      $parts = explode('=', $cookie);
      $name = trim($parts[0]);
      setcookie($name, '', time()-1000);
      setcookie($name, '', time()-1000, '/');
    }
  }
  header('Location: ./login.php');
}

if (isset($_POST['username']) && isset($_POST['password'])) {
  $login = new Login($_POST['username'], $_POST['password']);

  if (!$login->return()[0]) {
    $message = implode($login->return()[1]);
  } else {
    $login = true;

    header('Location: ./afterLogin.php');
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if ($Cache->getData('permissions_session', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Session', ['user' => NULL]]) && !$login) {
  header('Location: ./index.php');
  die;
}

$title = 'FileManager - Login';
$message = $message ?? '';

$template = $twig->load('login.html.twig');
echo $template->render([
  'title' => $title,
  'message' => $message,
  'debug' => $settings['debugging'],
]);
