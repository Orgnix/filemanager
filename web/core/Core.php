<?php

namespace FileManager;

use FileManager\Components\Filesystem;
use FileManager\Components\Database;
use FileManager\Settings;
use Symfony\Component\VarDumper\Cloner\Data;
use UserFramework\Components\User;

/**
 * FileManager Core methods
 */
class Core extends Settings {
  
  /** constant FILEMANAGER_VERSION */
  const FILEMANAGER_VERSION = '0.1 beta';

  /** @var \FileManager\Components\Filesystem $filesystem */
  protected $filesystem;

  /**
   * Core constructor
   */
  public function __construct() {
    parent::__construct();
    $this->database = new Database('OR');
    $this->filesystem = new Filesystem();
  }

  /**
   * Static function to check whether the installation was finished.
   */
  public static function isInstalled() {
    if (is_file('settings.php')) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  /**
   * Returns updates from server.
   */
  public function getUpdates() {
    $opts = [
      'http' => [
        'method' => 'GET',
        'header'=> 'x-fm-token: 234iuwefgjkwefg092u3urkgffd34@!);',
      ],
      "ssl" => [
        "verify_peer"=> false,
        "verify_peer_name"=> false,
      ],
    ];

    $context  = stream_context_create($opts);
    $updates = file_get_contents('https://mediaba.se/FMUPDATES.php', false, $context);
    
    return json_decode($updates);
  }

  /**
   * Pulls from repository, updating all components
   *
   * @return string
   */
  public function update() {
    $result = [];
    exec('git pull', $result);
    $return = '';
    foreach ($result as $line) {
      $return .= $line . '<br />';
    }
    return $return;
  }

  /**
   * Pulls from repository, updating all components
   *
   * @return string
   */
  public function updateInfo() {
    $result = [];
    exec('git status', $result);
    $return = '';
    foreach ($result as $line) {
      $return .= $line . '<br />';
    }
    return $return;
  }

  /**
   * Function to log data
   *
   * @param string $log
   *
   * @return bool
   */
  public function log($log) {
    $query = $this->database;
    $query->insert('logs')
          ->values([
      NULL,
      time(),
      $_SERVER['REMOTE_ADDR'],
      User::getUsername(),
      $log,
      $_SERVER['REQUEST_URI'],
    ]);
    if ($query->execute()) {
      $this->setStatus($log);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Lists all logs
   *
   * @param int $limit
   * @param string $sort_key
   * @param string $sort
   * @param string|null $search_term
   *
   * @return array|bool
   */
  public function getLogs($limit = 50, $sort_key = 'timestamp', $sort = 'DESC', $search_term = NULL) {
    $query = new Database('OR');
    $query->select('logs')
          ->fields(NULL, ['id', 'timestamp', 'ip', 'username', 'log', 'path'])
          ->orderBy($sort_key, $sort);
    if ($limit !== FALSE) {
      $query->limit(0, $limit);
    }
    if ($search_term !== NULL) {
      $query->condition('ip', '%' . $search_term . '%', 'LIKE')
            ->condition('username', '%' . $search_term . '%', 'LIKE')
            ->condition('log', '%' . $search_term . '%', 'LIKE')
            ->condition('path', '%' . $search_term . '%', 'LIKE');
    }
    if ($query->execute()) {
      return $query->fetchAllAssoc();
    }
    return FALSE;
  }

  /**
   * Removes all logs
   *
   * @return bool
   */
  public function removeLogs() {
    $query = $this->database;
    if ($query->delete('logs')->execute()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the last index
   *
   * @return int
   */
  public function getStatus() {
    $query = $this->database;
    $query->select('site')
          ->condition('setting', 'status')
          ->fields(NULL, ['value', 'timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      return $result[0];
    }
    return FALSE;
  }

  /**
   * Update status message
   *
   * @param string $message
   *
   * @return bool
   */
  public function setStatus($message) {
    $time = time();
    $query = $this->database;
    $query->update('site')
          ->condition('setting', 'status')
          ->values([
      'value' => $message,
      'timestamp' => $time,
    ]);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the timestamp of the last folder index
   *
   * @return int|bool
   */
  public function getLastIndex() {
    $query = $this->database;
    $query->select('site')
          ->condition('setting', 'last_index')
          ->fields(NULL, ['timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      return $result[0]['timestamp'];
    }
    return FALSE;
  }

  /**
   * Update the timestamp of the last folder index
   *
   * @param int $log
   *
   * @return bool
   */
  public function setLastIndex($log) {
    $query = $this->database;
    $query->update('site')
          ->condition('setting', 'last_index')
          ->values([
      'value' => $log,
      'timestamp' => time(),
    ]);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Index all folders
   *
   * @param string $path
   * @param bool $logged
   */
  public function indexFolders($path = NULL, $logged = FALSE) {
    if (($this->getLastIndex() + $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['index_frequency']) < time()) {
      if ($path !== NULL) { $path = '/' . $path; }
      $full_path = $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'] . $path;
      $folders = $this->filesystem->listFolderContent($full_path);
      foreach ($folders as $folder) {
        $folder['path'] = str_replace($path, '', $folder['path']);
        $files = $this->filesystem->listFolderFiles($full_path . $folder['path'], TRUE);
        foreach ($files as $file) {
          $extension = Filesystem::getFileExtension($file);
          if (in_array($extension, $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['file_ext']['image'])) {
            if (filesize($full_path . $folder['path'] . '/' . $file) > $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['image_size_limit']) {
              $this->filesystem->createThumbnail($full_path . $folder['path'], $file);
            }
          }
          if ($this->filesystem->isThumbnail($file)
              && !file_exists($full_path . $folder['path'] . '/' . $this->filesystem->getFilenameFromThumbnail($file))) {
            unlink($full_path . $folder['path'] . '/' . $file);
          }
        }
        if (count($folder) > 1) {
          $this->indexFolders($folder['path'], TRUE);
        }
      }
      if (!$logged) {
        $this->setLastIndex('Indexed folders [' . User::getUsername() . ']');
        $this->log('Indexed folders');
      }
    }
  }

  public function cron() {
    //$this->importSettings();
    //$this->setSettings();
    $this->indexFolders();
    $this->filesystem->DBFileCleanup();
    return $this->getStatus();
  }

  /**
   * Returns an easier-to-read string
   */
  public static function getHumanName($string) {
    $string = explode('_', $string);
    $string = implode(' ', $string);
    $string = ucfirst($string);

    return $string;
  }

}
