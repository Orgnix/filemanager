<?php

namespace FileManager;

use FileManager\Components\Database;

$dir = __DIR__;
$exploded_dir = explode('/', $dir);
array_pop($exploded_dir);
$dir_up = implode('/', $exploded_dir);

if (is_file($dir_up . '/settings.php')) {
  include_once($dir_up . '/settings.php');
} else {
  include_once($dir_up . '/default.settings.php');
}

/**
 * (Extension class) Settings
 */
class Settings {

  /** @var array $settings */
  protected $settings;

  /** @var array $settings_file */
  protected $settings_file;

  /** @var \FileManager\Components\Database $database */
  protected $database;

  /** @var \FileManager\Components\Cache $Cache */
  protected $Cache;

  /**
   * Settings constructor
   */
  public function __construct() {
    global $settings;
    global $Cache;
    $this->settings = $settings;
    $this->settings_file = $settings;
    $this->Cache = $Cache;
  }

  /**
   * Establish connection to DB
   */
  public function connect() {
    $this->database = new Database();
  }

  /**
   * Return Database var
   */
  public function database() {
    $this->connect();
    return $this->database;
  }

  /**
   * Returns settings array
   *
   * @param string $key
   *
   * @return array|bool $settings
   */
  public function getSetting($key) {
    $this->setSettings();
    if (isset($this->settings[$key])) {
      return $this->settings[$key];
    } elseif(isset($this->settings_file[$key])) {
      return $this->settings_file[$key];
    } else {
      return FALSE;
    }
  }

  /**
   * Returns settings array
   *
   * @return array $settings
   */
  public function getSettings() {
    $this->setSettings();
    return $this->settings;
  }

  /**
   * If database is filled, use that
   * Otherwise use the global var
   */
  public function setSettings() {
    if (count($this->getDatabaseValue()) > 0) {
      $this->settings = unserialize($this->getDatabaseValue()[0]['value']);
    }
  }

  /**
   * Checks if settings are imported
   *
   * @return array
   */
  public function getDatabaseValue() {
    $query = $this->database();
    $query->select('site')
      ->condition('setting', 'settings')
      ->condition('timestamp', 0, '>')
      ->fields(NULL, ['value'])
      ->execute();
    return $query->fetchAllAssoc();
  }

  /**
   * Import settings into database
   *
   * @param bool $override
   * @param array|null $array
   */
  public function importSettings($override = FALSE, $array = NULL) {
    if(count($this->getDatabaseValue()) === 0 || $override) {
      if ($array === NULL) {
        $settings = $this->settings_file;
      } else {
        $settings = $array;
      }

      if (isset($settings['excluded'])) {
        foreach ($settings['excluded'] as $key) {
          if (isset($settings[$key])) {
            unset($settings[$key]);
          }
        }
      }

      $query = $this->database();
      $query->update('site')
        ->condition('setting', 'settings')
        ->values([
          'value' => serialize($settings),
          'timestamp' => time(),
        ])->execute();

      $core = new Core();
      if ($array === NULL) {
        $core->log('Imported settings from settings file to database');
      } else {
        $core->log('Saved site settings');
      }
    }

  }

}
