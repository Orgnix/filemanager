<?php

namespace FileManager;

use FileManager\Components\Cache;

/**
 * Include Settings
 */
include_once(__DIR__ . '/Settings.php');

/**
 * Include UserFramework
 */
include_once(__DIR__ . '/UserFramework/includes.php');

/**
 * Include SqlFormatter
 */
include_once(__DIR__ . '/SqlFormatter/SqlFormatter.php');


/**
 * Include FileManager components
 */
include_once(__DIR__ . '/Core.php');
foreach (glob(__DIR__ . "/Components/*.php") as $filename) {
  if (is_file($filename)) {
    include_once($filename);
  }
}

$Cache = new Cache();

/**
 * Include custom functions
 */
include_once(__DIR__ . '/CreateUser.php');
include_once(__DIR__ . '/Login.php');
