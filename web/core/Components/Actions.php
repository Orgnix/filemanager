<?php

namespace FileManager\Components;

use FileManager\Settings;
use UserFramework\Components\User;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class Actions extends Settings {

  /** @var \FileManager\Components\Filesystem $filesystem */
	protected $filesystem;

  /** @var string $actionName */
	protected $actionName;

  /** @var array $variables */
	protected $variables;

  /** @var $user */
  protected $user;

  /** @var \Twig\Environment $twig */
	protected $twig;

  /** @var mixed $template */
	protected $template;

  /** @var string $template_folder */
  protected $template_folder;

  /**
   * Actions constructor
   */
  public function __construct(User $user) {
    parent::__construct();
    $this->filesystem = new Filesystem();
    $this->user = $user;
  }

  /**
   * Returns the name of the action
   *
   * @return string
   */
  public function getActionName() {
  	return $this->actionName;
  }

  /**
   * Sets the name of the action
   *
   * @param string $action_name
   *
   * @return static self
   */
  public function setActionName($action_name) {
  	$this->actionName = $action_name;
  	return $this;
  }

  /**
   * Sets themes subfolder
   *
   * @param string $folder
   *
   * @return static self
   */
  public function setTemplateFolder($folder) {
    $this->template_folder = $folder;
    return $this;
  }

  /**
   * Sets the variables for the action
   *
   * @param array $variables
   *
   * @return static self
   */
  public function setVariables($variables = []) {
  	$this->variables = $variables;
  	return $this;
  }

  /**
   * Returns all folders
   *
   * @return array
   */
	public function getAllFolders() {
		return $this->filesystem->listFolderContent($this->settings['root_folder']);
	}

  /**
   * Updates files in file_cache
   *
   * @param array $files
   *
   * @return bool
   */
  public function updateFilesCache($files = []) {
    $query = $this->database();
    $query->update('site')
          ->values([
            'value' => serialize($files),
            'timestamp' => time(),
          ])
          ->condition('setting', 'files_cache');
    if ($query->execute()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns twig template
   */
  public function loadTwigTemplate() {
    if ($this->template_folder !== NULL) {
      $this->template_folder .= '/';
    }
    $action_filename = strtolower($this->getActionName());
    $action_filename = str_replace(' ', '_', $action_filename);
    $this->twig = new Environment(
      new FilesystemLoader($this->settings['project_root'] . '/themes/' . $this->Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme'] . '/actions/' . $this->template_folder)
    );
	  $this->template = $this->twig->load($action_filename . '.html.twig');
  	return $this;
  }

  /**
   * Render function
   */
  public function render() {
		return $this->template->render($this->variables);
	}

}
