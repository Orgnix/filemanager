<?php

namespace FileManager\Components;

use FileManager\Settings;
use FileManager\Components\ViewMode;

/**
 * Assets class
 */
class Assets extends Settings {

  /** @var string $table */
  protected $table;

  /** @var string $field */
  protected $field;

  /** @var string $value */
  protected $value;

  /** @var array $condition */
  protected $condition;

  /** @var \FileManager\Components\ViewMode $viewModes */
  protected $viewModes;

  /**
   * UserExists Constructor.
   *
   * @param string $table
   * @param string $field
   * @param string $value
   * @param array $condition
   */
  public function __construct($table = '', $field = '', $value = '', $condition = []) {
    parent::__construct();
    $this->viewModes = new ViewMode();
    $this->table = $table;
    $this->field = $field;
    $this->value = $value;
    $this->condition = $condition;
    $this->connect();
  }

  /**
   * Returns assets from DB
   *
   * @return string $return
   */
  public function getAssets() {
    $query = $this->database;

    $query->select($this->table)
      ->fields(NULL, [$this->field])
      ->execute();

    if ($result = $query->fetchAllAssoc()) {
      $return = '';
      foreach ($result as $row) {
        $return .= $row[$this->field];
      }
      return $return;
    }

    return FALSE;
  }

  /**
   * Returns view mode assets
   *
   * @param string $view_mode
   * @param string $asset
   *
   * @return string
   */
  public function getViewModeAssets($view_mode, $asset) {
    $vm = $this->viewModes->getViewMode($view_mode);
    return $vm[0][$asset];
  }

  /**
   * Sets a value from an ajax request
   */
  public function setViewMode() {
    $query = $this->database;

    $query->select($this->table)
      ->fields(NULL, [$this->field]);
    foreach ($this->condition as $field => $value) {
      $query->condition($field, $value);
    }
    if (!$query->execute()) {
      return FALSE;
    }
    $result = $query->fetchAllAssoc();

    if (count($result) === 0) {
      $query->insert($this->table);
      $query->values([
        NULL,
        $this->condition['path'],
        $this->value,
      ]);
      if (!$query->execute()) {
        return FALSE;
      }
    } else {
      $query->update($this->table);
      foreach ($this->condition as $field => $value) {
        $query->condition($field, $value);
      }
      $query->values([
        'view_mode' => $this->value,
      ]);
      if (!$query->execute()) {
        return FALSE;
      }
    }
    return TRUE;
  }

}