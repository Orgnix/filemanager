<?php

namespace FileManager\Components;

use FileManager\Settings;
use UserFramework\Components\Permissions;

class Buttons extends Settings {

  /** @var array $tables */
  protected $tables;

  /** @var \UserFramework\Components\Permissions $permissions */
  protected $permissions;

  /**
   * UserExists Constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->tables = [
      'file' => 'file_buttons',
      'folder' => 'folder_buttons',
    ];

    $this->permissions = new Permissions();
    $this->connect();
  }

  protected function permissions() {
    return $this->permissions;
  }

  /**
   * Returns all data for view mode
   *
   * @param string $side
   *
   * @return bool
   */
  public function getAllButtons($table) {
    $buttons = $this->database;

    $buttons->select($this->tables[$table])
            ->fields(NULL, ['sort', 'value', 'onclick', 'class', 'css', 'js', 'permissions'])
            ->orderby('sort', 'ASC');

    if ($buttons->execute()) {
      $result = $buttons->fetchAllAssoc();
      $count = 0;
      foreach ($result as $row) {
        if (!$this->permissions()->hasPermission('Custom', [
            'user' => NULL,
            'permission' => $row['permissions'],
          ]
        )) {  unset($result[$count]); }
        $count++;
      }
      return $result;
    }

    return FALSE;
  }

}
