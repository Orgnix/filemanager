<?php

namespace FileManager\Components;

use FileManager\Settings;
use UserFramework\Components\User;
use ZipArchive;

class Filesystem extends Settings {

  /** @var array $excludedFolders */
  protected $excludedFolders;
  
  /**
   * Files constructor
   */
  public function __construct() {
    parent::__construct();
    $this->database = new Database('OR');
  }

  /**
   * Update path function
   *
   * @param string $path
   *
   * @return bool
   */
  public function updatePath($path) {
    $query = $this->database;
    $query->update('site')
          ->condition('setting', 'current_path')
          ->values([
            'value' => $path,
            'timestamp' => time(),
          ]);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }

  public static function copyDir($source, $dest) {
    $dir = opendir($source);
    @mkdir($dest);
    while(false !== ( $file = readdir($dir)) ) {
      if (( $file != '.' ) && ( $file != '..' )) {
        if ( is_dir($source . '/' . $file) ) {
          self::copyDir($source . '/' . $file,$dest . '/' . $file);
        }
        else {
          copy($source . '/' . $file,$dest . '/' . $file);
        }
      }
    }
    closedir($dir);
  }

  /**
   * Static function to get filesize
   *
   * @param string $path
   * @param bool $formatted
   *
   * @return array|string|bool
   */
  public static function getFileSize($path, $formatted = TRUE) {
    if (is_file($path)) {
      $size = filesize($path);
      if ($formatted) {
        return self::fileSizeFormatted($size);
      } else {
        return $size;
      }
    }

    return FALSE;
  }

  /**
   * Static function to get the time when file was created
   *
   * @param string $path
   * @param bool $formatted
   * @param string $full_format
   *
   * @return string|bool
   */
  public static function getFileCreationTime($path, $formatted = FALSE, $full_format = 'd/m/Y H:i') {
    if (is_file($path)) {
      if ($formatted) {
        $creation_time = filectime($path);
        $today = date('d/m/Y H:i');
        $this_year = date('Y');
        if (date($full_format, $creation_time) === $today) {
          return date('H:i', $creation_time);
        } elseif(date('Y', $creation_time) === $this_year) {
          return date('d/m H:i', $creation_time);
        } else {
          return date('d/m/Y', $creation_time);
        }
      } else {
        return filectime($path);
      }
    }

    return FALSE;
  }

  /**
   * Returns free disk space
   *
   * @param bool $formatted
   *
   * @return int|array
   */
  public static function getFreeDiskSpace($formatted = FALSE) {
    if ($formatted) {
      return self::fileSizeFormatted(disk_free_space(__DIR__));
    }
    return disk_free_space(__DIR__);
  }

  /**
   * Returns total disk space
   *
   * @param bool $formatted
   *
   * @return int|array
   */
  public static function getTotalDiskSpace($formatted = FALSE) {
    if ($formatted) {
      return self::fileSizeFormatted(disk_total_space(__DIR__));
    }
    return disk_total_space(__DIR__);
  }

  /**
   * Returns free disk space
   *
   * @param bool $formatted
   *
   * @return int|array
   */
  public static function getUsedDiskSpace($formatted = FALSE) {
    if ($formatted) {
      return self::fileSizeFormatted(disk_total_space(__DIR__) - disk_free_space(__DIR__));
    }
    return disk_total_space(__DIR__) - disk_free_space(__DIR__);
  }

  /**
   * Returns an integer into a readable file format
   * 
   * @param int $size
   * 
   * @return array
   */
  public static function fileSizeFormatted($size) {
    if ($size >= 1099511627776) {
      return [round($size / 1024 / 1024 / 1024 / 1024, 2), 'TB'];
    } elseif ($size >= 1073741824) {
      return [round($size / 1024 / 1024 / 1024, 2), 'GB'];
    } elseif ($size >= 1048576) {
      return [round($size / 1024 / 1024, 1), 'MB'];
    } elseif ($size >= 1024) {
      return [round($size / 1024), 'kB'];
    } else {
      return [$size, 'B'];
    }
  }

  /**
   * Cleanup file entries in DB
   *
   * @return bool
   */
  public function DBFileCleanup() {
    $query = $this->database;
    $query->select('files')
          ->fields(NULL, ['id', 'path', 'filename']);
    if($query->execute()) {
      $result = $query->fetchAllAssoc();
      foreach ($result as $file) {
        if (!file_exists($file['path'] . '/' . $file['filename'])) {
          $this->delFileFromDB($file['path'], $file['filename']);
        }
      }
    }
    return FALSE;
  }
  
  /**
   * Is file already an entry in the database
   *
   * @param string $path
   * @param string $filename
   *
   * @return bool
   */
  public function isFileInDB($path, $filename) {
    $query = new Database('AND');
    $query->select('files')
          ->condition('path', $path)
          ->condition('filename', $filename)
          ->fields(NULL, ['id'])
          ->execute();
    if ($result = $query->fetchAllAssoc()) {
      if (count($result) > 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Adds file entry to the database
   *
   * @param string $path
   * @param string $filename
   *
   * @return bool
   */
  public function addFileToDB($path, $filename) {
    if (!$this->isFileInDB($path, $filename)) {
      $query = $this->database;
      $query->insert('files')
        ->values([
          'id' => NULL,
          'path' => $path,
          'filename' => $filename,
        ]);
      if ($query->execute()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Deletes file entry from the database
   *
   * @param string $path
   * @param string $filename
   *
   * @return bool
   */
  public function delFileFromDB($path, $filename) {
    if (!$this->isFileInDB($path, $filename)) {
      $query = $this->database;
      $query->delete('files')
            ->condition('path', $path)
            ->condition('filename', $filename);
      if ($query->execute()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Function that writes content to file
   *
   * @param string $filepath
   * @param string $filecontent
   *
   * @return bool
   */
  public function writeToFile($filepath, $filecontent) {
    if (is_file($filepath)) {
      $this->fileRevision($filepath, file_get_contents($filepath));
      if ($file = fopen($filepath, 'w')) {
        fwrite($file, $filecontent);
        fclose($file);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Creates revision for a file and writes it to the database
   *
   * @param string $filepath
   * @param string $filecontent
   *
   * @return bool
   */
  public function fileRevision($filepath, $filecontent) {
    if ($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['create_revisions']) {
      $query = $this->database;
      $query->insert('file_revisions')
            ->values([
              'id' => NULL,
              'timestamp' => time(),
              'author' => User::getUsername(),
              'file_path' => $filepath,
              'revision_content' => $filecontent,
            ]);
      if ($query->execute()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Returns folders excluded from indexing
   */
  public function setExcludedFolders() {
    $this->excludedFolders = $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['folders']['excluded_folders'];
    if (!is_array($this->excludedFolders)) {
      $this->excludedFolders = [];
    }
  }

  /**
   * Is folder excluded
   *
   * @param string $path
   *
   * @return bool
   */
  public function isFolderExcluded($path) {
    foreach ($this->excludedFolders as $folder) {
      if ($path == $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'] . $folder) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Function for recursive directory file list search as an array.
   *
   * @param mixed $dir Main Directory Path.
   *
   * @return array
   */
  public function listFolderContent($dir) {
    $fileInfo = scandir($dir);
    $allFileLists = [];
    if ($this->excludedFolders == '') {
      $this->setExcludedFolders();
    }

    foreach ($fileInfo as $folder) {
      if ($folder !== '.' && $folder !== '..') {
        $path = $dir . DIRECTORY_SEPARATOR . $folder;
        if (is_dir($path) === true && !$this->isFolderExcluded($path)) {
          $allFileLists[$folder] = $this->listFolderContent($path);
          $allFileLists[$folder]['path'] = str_replace($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'], '', $path);
        }
      }
    }

    return $allFileLists;
  }

  /**
   * Function for recursive directory file list search as an array.
   *
   * @param mixed $dir Main Directory Path.
   * @param bool $thumbnail_override
   *
   * @return array
   */
  public function listFolderFiles($dir, $thumbnail_override = FALSE) {
    $fileInfo     = scandir($dir);
    $allFileLists = [];

    foreach ($fileInfo as $file) {
      if (strpos($file, $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix']) === FALSE || $thumbnail_override) {
        if ($file !== '.' && $file !== '..') {
          if (!is_dir($dir . DIRECTORY_SEPARATOR . $file)) {
            $allFileLists[] = $file;
          }
        }
      }
    }

    return $allFileLists;
  }

  /**
   * Returns the extension of the given file.
   *
   * @param string $file
   *
   * @return string
   */
  public static function getFileExtension($file) {
    $exploded_file = explode('.', $file);
    return array_pop($exploded_file);
  }

  /**
   * Checks if file is thumbnail
   * 
   * @param string $file
   * 
   * @return bool
   */
  public function isThumbnail($file) {
    $thumbnail_prefix = $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'];
    if (strpos($file, $thumbnail_prefix) !== FALSE) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns full filename from given thumbnail filename
   * 
   * @param string $thumbnail
   *
   * @return string
   */
  public function getFilenameFromThumbnail($thumbnail) {
    $thumbnail_prefix = $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'];
    return str_replace($thumbnail_prefix, '', $thumbnail);
  }

  /**
   * Removes thumbnail if exists
   *
   * @param string $path
   * @param string $filename
   *
   * @return bool
   */
  public function removeThumbnail($path, $filename) {
    if (file_exists($path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename)) {
      unlink($path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Creates thumbnail if filesize is over specified filesize
   *
   * @param string $path
   * @param string $filename
   *
   * @return bool
   */
  public function createThumbnail($path, $filename) {
    $file = $path . '/' . $filename;
    $file_exploded = explode('.', $filename);
    $source_properties = getimagesize($file);

    $image_type = $source_properties[2];
    if ($image_type == IMAGETYPE_JPEG) {
      $image_resource_id = imagecreatefromjpeg($file);
    } elseif($image_type == IMAGETYPE_GIF)  {  
      $image_resource_id = imagecreatefromgif($file);
    } elseif($image_type == IMAGETYPE_PNG) {
      $image_resource_id = imagecreatefrompng($file);
    }

    if ($source_properties[0] > $source_properties[1]) {
      $width = 248;
      $height = $source_properties[1] / $source_properties[0] * 248;
    } elseif ($source_properties[0] < $source_properties[1]) {
      $height = 248;
      $width = $source_properties[0] / $source_properties[1] * 248;
    }
    $target_layer = imagecreatetruecolor($width,$height);

    imagecopyresampled($target_layer,$image_resource_id,0,0,0,0,$width,$height, $source_properties[0],$source_properties[1]);

    if (file_exists($path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename) !== TRUE) {
      if ($image_type == IMAGETYPE_JPEG) {
        return imagejpeg($target_layer, $path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename);
      } elseif($image_type == IMAGETYPE_GIF)  {  
        return imagegif($target_layer, $path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename);
      } elseif($image_type == IMAGETYPE_PNG) {
        return imagepng($target_layer, $path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename);
      }
    }

    return FALSE;
  }

  /**
   * Static function to return the thumbnail path
   */
  public function getThumbnailPath($path, $filename) {
    if (in_array(self::getFileExtension($path . '/' . $filename), $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['file_ext']['image'])) {
      if (file_exists($path . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename)) {
        return str_replace($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'], $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['folder_as_uri'], $path) . '/' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['thumbnail_prefix'] . $filename;
      } else {
        return str_replace($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'], $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['folder_as_uri'], $path) . '/' . $filename;
      }
    }

    return FALSE;
  }

  /**
   * Returns all extensions in a folder
   * 
   * @param array $dir
   *
   * @return array
   */
  public function getExtensionsInFolder($dir) {
    $extensions = [];
    foreach ($dir as $item) {
      $exploded = explode('.', $item);
      if (!in_array(strtolower(end($exploded)), $extensions)) {
        $extensions[] = end($exploded);
      }
    }

    return $extensions;
  }

  /**
   * Return the folders as a html list
   * 
   * @param  boolean $files
   *
   * @return mixed
   */
  public function listFilesystem($files = TRUE) {
    return $this->HtmlListFiles($this->listFolderContent($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder']), $files);
  }

  /**
   * Return the folders as a html list
   * 
   * @param array $arr
   * @param boolean $files
   *
   * @return string
   */
  public function HtmlListFiles($arr, $files = FALSE) {
    $retStr = '<ul class="folders">';
    if (is_array($arr)) {
      foreach ($arr as $key=>$val) {
        if ($key === 'root') { $val['path'] = '/'; }
        if (is_array($val)) {
          if (!isset($_GET['path']) || (isset($_GET['path']) && $_GET['path'] !== $val['path'])) {
            $retStr .= '<li><a href="?path=' . $val['path'] . '">' . $key . '</a>' . $this->HtmlListFiles($val, $files) . '</li>';
          } else {
            $retStr .= '<li>' . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['selected_item_prefix'] .
              $key . $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['selected_item_suffix'] . $this->HtmlListFiles($val, $files) . '</li>';
          }
        }
      }

      if ($files) {
        foreach ($arr as $key=>$val) {
          if (!is_array($val)) {
            $retStr .= '<li>' . $key . '</li>';
          }
        }
      }
    }
    $retStr .= '</ul>';

    return $retStr;
  }

   /**
   * StackOptions > stacks options for select html tag
   * 
   * @param  array   $option
   * @param  integer $indent_amount
   * @param  string  $indent_char
   *
   * @return string
   */
  public function StackOptions($option, $indent_amount, $indent_char = "--", $path = '') {
    $indenting = '';
    $retStr = '';
    for ($i=1;$i<$indent_amount;$i++) {
      $indenting .= '&nbsp;&nbsp;';
    }
    $indenting .= $indent_char;
    foreach ($option as $key=>$value) {
      $selected = '';
      if (isset($value['path'])) {
        if ($path === $value['path']) { $selected = ' SELECTED'; }
      }
      if (is_array($value) && count($value) > 1) {
        $retStr .= '<option value="' . $value['path'] . '"' . $selected . '>' . $indenting . ' ' . $key . '</option>' . $this->StackOptions($value, $indent_amount+1, $indent_char, $path);
      } else {
        if ($key !== 'path') {
          $retStr .= '<option value="' . $value['path'] . '"' . $selected . '>' . $indenting . ' ' . $key . '</option>';
        }
      }
    }

    return $retStr;
  }
  
  /**
   * Actions:Copy - Copies files defined in database from the old to the new path
   *
   * @return bool
   */
  public function Copy() {
    $query = $this->database;
    $query->select('site')
          ->fields(NULL, ['setting', 'value', 'timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      $results = [];
      foreach($result as $id => $values) {
        if ($values['setting'] == 'files_cache') {
          $results['files_cache'] = unserialize($values['value']);
        } elseif ($values['setting'] == 'current_path') {
          $results['current_path'] = $values['value'];
        } elseif ($values['setting'] == 'new_path') {
          $results['new_path'] = $values['value'];
        }
      }
      foreach ($results['files_cache'] as $file) {
        $file['name'] = str_replace('%20', ' ', $file['name']);
        if (!copy($results['current_path'] . '/' . $file['name'], $results['new_path'] . '/' . $file['name'])) {
          return FALSE;
        }
      }
      return TRUE;
    }
    
    return FALSE;
  }
  
  /**
   * Actions:Move - Copies files defined in database from the old to the new path
   *
   * @return bool
   */
  public function Move() {
    $query = $this->database;
    $query->select('site')
          ->fields(NULL, ['setting', 'value', 'timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      $results = [];
      foreach($result as $id => $values) {
        if ($values['setting'] == 'files_cache') {
          $results['files_cache'] = unserialize($values['value']);
        } elseif ($values['setting'] == 'current_path') {
          $results['current_path'] = $values['value'];
        } elseif ($values['setting'] == 'new_path') {
          $results['new_path'] = $values['value'];
        }
      }
      foreach ($results['files_cache'] as $file) {
        $file['name'] = str_replace('%20', ' ', $file['name']);
        if (!rename($results['current_path'] . '/' . $file['name'], $results['new_path'] . '/' . $file['name'])) {
          return FALSE;
        }
      }
      return TRUE;
    }
    
    return FALSE;
  }
  
  /**
   * Actions:Delete - Copies files defined in database from the old to the new path
   *
   * @return bool
   */
  public function Delete() {
    $query = $this->database;
    $query->select('site')
          ->fields(NULL, ['setting', 'value', 'timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      $results = [];
      foreach($result as $id => $values) {
        if ($values['setting'] == 'files_cache') {
          $results['files_cache'] = unserialize($values['value']);
        } elseif ($values['setting'] == 'current_path') {
          $results['current_path'] = $values['value'];
        }
      }
      foreach ($results['files_cache'] as $file) {
        $file['name'] = str_replace('%20', ' ', $file['name']);
        if (!unlink($results['current_path'] . '/' . $file['name'])) {
          return FALSE;
        }
      }
      return TRUE;
    }
    
    return FALSE;
  }
  
  /**
   * Actions:Compress - Copies files defined in database from the old to the new path
   *
   * @param string $compressed_file_name
   *
   * @return bool
   */
  public function CompressZip($compressed_file_name) {
    $zip = new \ZipArchive();
    $query = $this->database;
    $query->select('site')
          ->fields(NULL, ['setting', 'value', 'timestamp']);
    if ($query->execute()) {
      $result = $query->fetchAllAssoc();
      $results = [];
      foreach($result as $id => $values) {
        if ($values['setting'] == 'files_cache') {
          $results['files_cache'] = unserialize($values['value']);
        } elseif ($values['setting'] == 'current_path') {
          $results['current_path'] = $values['value'];
        }
      }
      if ($zip->open($results['current_path'] . '/' . $compressed_file_name, ZipArchive::CREATE) === TRUE) {
        foreach ($results['files_cache'] as $file) {
          $file['name'] = str_replace('%20', ' ', $file['name']);
          if (!$zip->addFile($results['current_path'] . '/' . $file['name'])) {
            return FALSE;
          }
        }
        $zip->close();
        return TRUE;
      }
    }
    
    return FALSE;
  }

}
