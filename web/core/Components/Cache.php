<?php

namespace FileManager\Components;

use FileManager\Settings;

/**
 * Class Cache
 *
 * @package FileManager
 */
class Cache extends Settings {

  /** @var array $cacheableData */
  protected $cacheableData;

  /** @var array $cacheStats */
  protected $cacheStats;

  /**
   * Checks if data is in cache, if so read it from cache.
   * If data not in cache, use fallback and add it to cache.
   *
   * @param $cacheKey
   * @param string $fallbackClass
   * @param string $fallbackMethod
   * @param array $fallbackData
   * @param array $classData
   *
   * @return mixed
   */
  public function getData($cacheKey, $fallbackClass = '', $fallbackMethod = '', $fallbackData = [], $classData = []) {
    if (!isset($this->cacheableData[$cacheKey])) {
      $class = new $fallbackClass(...$classData);
      $this->cacheableData[$cacheKey] = $class->{$fallbackMethod}(...$fallbackData);
      if (!isset($this->cacheStats[$cacheKey])) {
        $this->cacheStats[$cacheKey]['created'] = 0;
        $this->cacheStats[$cacheKey]['called'] = 0;
      }
      $this->cacheStats[$cacheKey]['created']++;
    }
    $this->cacheStats[$cacheKey]['called']++;
    return $this->cacheableData[$cacheKey];
  }

  /**
   * Returns the whole cache array.
   *
   * @return array
   */
  public function returnCache() {
    return $this->cacheableData;
  }

  /**
   * Returns the whole cache array.
   *
   * @return array
   */
  public function returnCacheStats() {
    return $this->cacheStats;
  }

  /**
   * Updates new path cache in database
   *
   * @param string $folder
   *
   * @return bool
   */
  public function updateNewPathCache($folder) {
    $query = $this->database();
    $folder = $this->getData('settings', '\\FileManager\\Settings', 'getSettings')['root_folder'] . $folder;
    $query->update('site')
      ->values([
        'value' => $folder,
        'timestamp' => time(),
      ])
      ->condition('setting', 'new_path');
    return $query->execute();
  }

}