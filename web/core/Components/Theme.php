<?php

namespace FileManager\Components;

use FileManager\Settings;

class Theme extends Settings {

  /** @var string|null $theme */
  protected $theme;

  /** @var array $themes */
  protected $themes;

  /**
   * Theme constructor.
   *
   * @param string|null $theme
   */
  public function __construct($theme = NULL) {
    parent::__construct();
    $this->theme = $theme;
  }

  /**
   * Returns list of all themes
   */
  public function getThemes() {
    $folder = $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['project_root'] . '/themes';

    if ($handle = opendir($folder)) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          $this->themes[$entry] = $this->getThemeInfo($entry);
        }
      }
      closedir($handle);
    }
    return $this->themes;
  }

  /**
   * Get theme info
   *
   * @param string $theme
   *
   * @return mixed
   */
  public function getThemeInfo($theme = NULL) {
    if ($theme === NULL) {
      $theme = $this->theme;
    }
    if ($theme === NULL) {
      return FALSE;
    }
    $file = file_get_contents($this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['project_root'] . '/themes/' . $theme . '/' . $theme . '.xml');
    $xml = simplexml_load_string($file);
    $json = json_encode($xml);
    return json_decode($json);
  }

}
