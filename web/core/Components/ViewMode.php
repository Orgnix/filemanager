<?php

namespace FileManager\Components;

use FileManager\Settings;

class ViewMode extends Settings {

  /** @var string $fs_table */
  protected $fs_table;

  /** @var $vm_table */
  protected $vm_table;

  /**
   * UserExists Constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->fs_table = 'folder_settings';
    $this->vm_table = 'view_modes';

    $this->connect();
  }
  
  /**
   * Saves view mode
   *
   * @param string $vm
   * @param array $values
   *
   * @return bool
   */
  public function save($vm, $values) {
    $query = $this->database();
    $query->update($this->vm_table)
          ->values($values)
          ->condition('view_mode', $vm);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }
  
  /**
   * Adds view mode
   *
   * @param array $values
   *
   * @return bool
   */
  public function add($values) {
    $query = $this->database();
    $query->insert($this->vm_table)
          ->values($values);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }
  
  /**
   * Adds view mode
   *
   * @param string $vm
   *
   * @return bool
   */
  public function remove($vm) {
    $query = $this->database();
    $query->delete($this->vm_table)
          ->condition('view_mode', $vm);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns all data for view mode
   *
   * @return bool
   */
  public function getAllViewModes() {
    $view_mode = $this->database;

    $view_mode->select($this->vm_table)
              ->fields(NULL, ['id', 'view_mode', 'max_fn_length', 'prefix', 'suffix', 'code', 'css', 'js'])
              ->execute();

    if ($result = $view_mode->fetchAllAssoc()) {
      return $result;
    }

    return FALSE;
  }

  /**
   * Returns all data for view mode
   *
   * @return bool
   */
  public function getViewMode($vm) {
    $view_mode = $this->database;

    $view_mode->select($this->vm_table)
              ->condition('view_mode', $vm)
              ->fields(NULL, ['id', 'view_mode', 'max_fn_length', 'prefix', 'suffix', 'code', 'css', 'js'])
              ->execute();

    if ($result = $view_mode->fetchAllAssoc()) {
      return $result;
    }

    return FALSE;
  }

  /**
   * Returns view mode from folder settings
   * 
   * @param string $folder
   *
   * @return bool
   */
  public function getFolderViewMode($folder) {
    $folder_settings = $this->database;

    $folder_settings->select($this->fs_table)
                    ->condition('path', $folder)
                    ->fields(NULL, ['view_mode'])
                    ->execute();

    $result = $folder_settings->fetchAllAssoc();
    if (isset($result[0])) {
      return $this->getViewMode($result[0]['view_mode']);
    }

    // If the query returned 0 results, return FALSE
    return $this->getViewMode('list');
  }

  /**
   * Sets view-mode for a folder
   *
   * @param string $folder
   * @param string $view_mode
   *
   * @return bool
   */
  public function setFolderViewMode($folder, $view_mode) {
    $query = $this->database;
    if (!$this->folderSettingExists($folder)) {
      $query->insert($this->fs_table)
            ->values([NULL, $folder, $view_mode]);
    } else {
      $query->update($this->fs_table)
            ->condition('path', $folder)
            ->values(['view_mode' => $view_mode]);
    }

    if($query->execute()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if folder setting exists
   * 
   * @param string $folder
   * 
   * @return bool
   */
  public function folderSettingExists($folder) {
    $folder_settings = $this->database;

    $folder_settings->select($this->fs_table)
                    ->condition('path', $folder)
                    ->fields(NULL, ['view_mode'])
                    ->execute();

    $result = $folder_settings->fetchAllAssoc();
    if (isset($result[0])) {
      if (count($result[0]) > 0) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
