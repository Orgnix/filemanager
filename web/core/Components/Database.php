<?php

namespace FileManager\Components;

use FileManager\Settings;
use FileManager\SqlFormatter;

class Database extends Settings {

  /** @var array $db */
  protected $db;

  /** @var mixed $database */
  protected $database;

  /** @var mixed $result */
  protected $result;

  /** @var array $query */
  protected $query;

  /** @var string $method */
  protected $method;

  /** @var array $tables */
  protected $tables;

  /** @var array $fields */ 
  protected $fields;

  /** @var array $values */ 
  protected $values;

  /** @var array $joins */
  protected $joins;

  /** @var array $orderby */
  protected $orderby;

  /** @var string $limit */
  protected $limit;

  /** @var array $conditions */
  protected $conditions;

  /** @var string $condition_delimiter */
  protected $condition_delimiter;
	
  /**
   * Database constructor
   *
   * @param string $condition_delimiter
   *      This is the condition delimiter you wish to use in your WHERE clause
   * @param string $database
   *      With this you can override the used database
   */
  public function __construct($condition_delimiter = 'AND', $database = NULL) {
    parent::__construct();
    $this->tables = [];
    $this->fields = [];
    $this->joins = [];
    $this->values = [];
    $this->orderby = [];
    $this->conditions = [];
    $this->condition_delimiter = $condition_delimiter;
    $this->query = [
        'useQuery' => FALSE,
        'query' => [],
    ];
    $this->joins = [
        'isJoined' => FALSE,
        'join' => [],
    ];

    $this->db = [];
    $this->db['host'] = $this->settings['database']['hostname'];
    $this->db['user'] = $this->settings['database']['username'];
    $this->db['pass'] = $this->settings['database']['password'];
    $this->db['dbname'] = $database ?? $this->settings['database']['database'];

    $this->connect();
  }

  /**
   * connect function
   */
  public function connect() {
    $this->database = new \mysqli($this->db['host'], $this->db['user'], $this->db['pass'], $this->db['dbname']);
  }

  /**
   * Static escape function
   * 
   * @param \mysqli $connection
   * @param string $value
   *
   * @return string
   */
  public static function Cleanse($connection, $value) {
    return mysqli_real_escape_string($connection, $value);
  }

  /**
   * Static function to add single quotation marks in case
   * this was not done when building the query.
   *
   * @param string $value
   *
   * @return string
   */
  public static function addQuotationMarks($value) {
    if(!is_null($value)) {
      if (substr($value, 0, 1) !== "'" && substr($value, -1, 1) !== "'") {
        return "'" . $value . "'";
      }
    }

    return $value;
  }

  /**
   * Query function
   *
   * @param string $query
   *
   * @return mixed|bool
   */
  public function query($query) {
    if ($this->settings['debugging']) {
      krumo($query);
    }
    if ($result = $this->database->query($query)) {
      $this->query = [
        'useQuery' => TRUE,
        'query' => $result,
      ];
      return $this;
    }

    return FALSE;
  }
  
  /**
   * select method
   * 
   * @param string $table
   * @param string $alias
   * @param array $options
   *
   * @return self
   */
  public function select($table, $alias = NULL, $options = NULL) {
    $this->method = 'SELECT';
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
    return $this;
  }
  
  /**
   * insert method
   * 
   * @param string $table
   * @param string $alias
   * @param array $options
   *
   * @return self
   */
  public function insert($table, $alias = NULL, $options = NULL) {
    $this->method = 'INSERT';
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
    return $this;
  }
  
  /**
   * update method
   * 
   * @param string $table
   * @param string $alias
   * @param array $options
   *
   * @return self
   */
  public function update($table, $alias = NULL, $options = NULL) {
    $this->method = 'UPDATE';
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
    return $this;
  }
  
  /**
   * delete method
   * 
   * @param string $table
   * @param string $alias
   * @param array $options
   *
   * @return self
   */
  public function delete($table, $alias = NULL, $options = NULL) {
    $this->method = 'DELETE';
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
    return $this;
  }
  
  /**
   * join method
   * 
   * @param string $table
   * @param string $alias
   * @param array $options
   *
   * @return self
   */
  public function join($table, $alias = NULL, $options = NULL) {
    $this->tables[] = ['table' => $table, 'alias' => $alias, 'options' => $options];
    return $this;
  }

  /**
   * condition method
   * 
   * @param string $field
   * @param string $value
   * @param string $operator
   *
   * @return self
   */
  public function condition($field, $value = NULL, $operator = '=') {
    $this->conditions[] = [
      'field' => $field,
      'operator' => $operator,
      'value' => $value,
    ];
    return $this;
  }

  /**
   * fields method
   * 
   * @param string $table_alias
   * @param array $fields
   *
   * @return self
   */
  public function fields($table_alias, $fields = []) {
    if ($this->method === 'SELECT') {
      $this->fields[] = ['table_alias' => $table_alias, 'fields' => $fields];
    }
    return $this;
  }

  /**
   * values method
   * 
   * @param array $data
   *
   * @return self
   */
  public function values($data = []) {
    if ($this->method === 'INSERT') {
      foreach($data as $field => $value) {
        $this->values[] = $value;
      }
    } elseif($this->method === 'UPDATE') {
      $this->values = $data;
    }
    return $this;
  }

  /**
   * orderBy method
   * 
   * @param string $field
   * @param string $direction
   *
   * @return self
   */
  public function orderBy($field, $direction) {
    if ($this->method === 'SELECT') {
      $this->orderby[] = ['field' => $field, 'direction' => $direction];
    }
    return $this;
  }

  /**
   * limit method
   * 
   * @param int $start
   * @param int $end
   *
   * @return self
   */
  public function limit($start = 0, $end = 15) {
    $this->limit = "$start, $end";
    return $this;
  }

  /**
   * Builds the query if method is SELECT
   * 
   * @return string
   */
  protected function buildSelectQuery() {
    $tables = '';
    foreach ($this->tables as $table) {
      if ($tables !== '') { $tables .= ', '; }
      $tables .= $table['table']; 
      if ($table['alias'] !== NULL) {
        $tables .= ' AS ' . $table['alias'];
      }
    }
    $fields = '';
    foreach ($this->fields as $field_array) {
      foreach ($field_array['fields'] as $field) {
        if ($fields !== '') { $fields .= ', '; }
        if (!is_null($field_array['table_alias'])) {
          $fields .= $field_array['table_alias'] . '.' . self::Cleanse($this->database,$field);
        } else {
          $fields .= self::Cleanse($this->database,$field);
        }
      }
    }
    $conditions = '';
    foreach ($this->conditions as $condition) {
      if ($conditions !== '') { $conditions .= ' ' . $this->condition_delimiter . ' '; }
      $conditions .= $condition['field'] . ' ' . $condition['operator'] . ' ' . self::addQuotationMarks(self::Cleanse($this->database, $condition['value']));
    }
    $orderby = '';
    foreach ($this->orderby as $order) {
      if ($orderby !== '') { $orderby .= ', '; }
      $orderby .= $order['field'] . ' ' . $order['direction'];
    }
    if ($conditions !== '') {
      $conditions = ' WHERE ' . $conditions;
    }
    if ($orderby !== '') {
      $orderby = ' ORDER BY ' . $orderby;
    }
    if ($fields === '') {
      $fields = '*';
    }
    $limit = '';
    if ($this->limit !== NULL) {
      $limit = ' LIMIT ' . $this->limit;
    }
    $query = 'SELECT ' . $fields . ' FROM ' . $tables . $conditions . $orderby . $limit;

    return $query;
  }

  /**
   * Builds the query if method is INSERT
   * 
   * @return string
   */
  protected function buildInsertQuery() {
    $tables = '';
    foreach ($this->tables as $table) {
      if ($tables !== '') { $tables .= ', '; }
      $tables .= $table['table'];
    }
    $values = '';
    foreach ($this->values as $value) {
      if ($values !== '') { $values .= ', '; }
      $values .= self::addQuotationMarks(self::Cleanse($this->database, $value));
    }
    $query = 'INSERT INTO ' . $tables . ' VALUES (' . $values . ')';

    return $query;
  }

  /**
   * Builds the query if method is UPDATE
   * 
   * @return string
   */
  protected function buildUpdateQuery() {
    $tables = '';
    foreach ($this->tables as $table) {
      if ($tables !== '') { $tables .= ', '; }
      $tables .= $table['table']; 
      if ($table['alias'] !== NULL) {
        $tables .= ' AS ' . $table['alias'];
      }
    }
    $fields = '';
    foreach ($this->values as $field => $value) {
      if ($fields !== '') { $fields .= ', '; }
      $fields .= $field . ' = ' . self::addQuotationMarks(self::Cleanse($this->database,$value));
    }
    $conditions = '';
    foreach ($this->conditions as $condition) {
      if ($conditions !== '') { $conditions .= ' ' . $this->condition_delimiter . ' '; }
      $conditions .= $condition['field'] . ' ' . $condition['operator'] . ' ' . self::addQuotationMarks(self::Cleanse($this->database, $condition['value']));
    }
    if ($conditions !== '') {
      $conditions = ' WHERE ' . $conditions;
    }
    if ($fields === '') {
      $fields = '*';
    }
    $query = 'UPDATE ' . $tables . ' SET ' . $fields . $conditions;

    return $query;
  }

  /**
   * Builds the query if method is DELETE
   * 
   * @return string
   */
  protected function buildDeleteQuery() {
    $tables = '';
    foreach ($this->tables as $table) {
      if ($tables !== '') { $tables .= ', '; }
      $tables .= $table['table'];
    }
    $conditions = '';
    foreach ($this->conditions as $condition) {
      if ($conditions !== '') { $conditions .= ' ' . $this->condition_delimiter . ' '; }
      $conditions .= $condition['field'] . ' ' . $condition['operator'] . ' ' . self::addQuotationMarks(self::Cleanse($this->database, $condition['value']));
    }
    if ($conditions !== '') {
      $conditions = ' WHERE ' . $conditions;
    }
    $query = 'DELETE FROM ' . $tables . $conditions;

    return $query;
  }

  /**
   * execute query method
   * 
   * @return bool
   */
  public function execute() {
    if (is_array($this->query) && $this->query['useQuery'] === TRUE) {
      if ($this->result = $this->query['result']) {
        return TRUE;
      }
    } else {
      $query = '';
      if ($this->method === 'SELECT') {
        $query = $this->buildSelectQuery();
      } elseif ($this->method === 'INSERT') {
        $query = $this->buildInsertQuery();
      } elseif ($this->method === 'UPDATE') {
        $query = $this->buildUpdateQuery();
      } elseif ($this->method === 'DELETE') {
        $query = $this->buildDeleteQuery();
      }

      if ($this->settings['debugging']) {
        echo '<pre style="color:#fff;">';
        echo SqlFormatter::format($query);
        echo '</pre>';
      }
      
      $result = $this->database->query($query);
      if ($result !== FALSE) {
        $this->result = $result;

        $this->reset();
        return TRUE;
      }
    }
    $this->reset();
    return FALSE;
  }

  /**
   * fetchAllAssoc method
   *
   * @param string|null $key
   * 
   * @return array|bool
   */
  public function fetchAllAssoc($key = NULL) {
    if ($this->result !== FALSE) {
      $records = [];
      while($record = mysqli_fetch_assoc($this->result)) {
        if ($key !== NULL) {
          $records[$record[$key]] = $record;
        } else {
          $records[] = $record;
        }
      }

      $this->result = '';
      return $records;
    }

    $this->result = '';
    return FALSE;
  }

  public function reset() {
    $this->tables = [];
    $this->fields = [];
    $this->joins = [];
    $this->values = [];
    $this->orderby = [];
    $this->conditions = [];
    $this->method = '';
    $this->query = [
        'useQuery' => FALSE,
        'query' => [],
    ];
    $this->joins = [
        'isJoined' => FALSE,
        'join' => [],
    ];
  }

}
