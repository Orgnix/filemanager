<?php

namespace FileManager;

/**
 * Make use of all the necessary classes
 */
use UserFramework\Components\UserExists;
use UserFramework\Components\Password;
use UserFramework\Components\Session;
use FileManager\Settings;

/**
 * Login class
 */
class Login extends Settings {

  /** @var array $user */
  protected $user;

  /** @var array $logs */
  protected $logs;

  /** @var array $errors */
  protected $errors;

  /** @var array $return_array */
  protected $return_array;

  /** @var \UserFramework\Components\Session $session */
  protected $session;

  /** @var \UserFramework\Components\Password $password */
  protected $password;

  /** @var \UserFramework\Components\UserExists $userExists */
  protected $userExists;

  /**
   * CreateUser constructor
   * 
   * @param string $username
   * @param string $password
   * @param string $email
   */
  public function __construct($username, $password) {
    parent::__construct();
    $this->user['username'] = $username;
    $this->user['password'] = $password;
    $this->errors = [];
    $this->logs = [];
    $this->return_array = [];

    $this->session = new Session(TRUE);
    $this->password = new Password();

    $this->connect();

    $this->userExists = new UserExists($this->database, ['username' => $this->user['username']], 'unique_id', 'users');
    
    if ($this->password->securePassword($this->user['password'])[0]) {
      $userexists = $this->userExists->CheckIfExists();
      if ($userexists[0]) {
        if ($this->password->checkPassword($this->user['password'], $userexists[1]['password'])[1]) {
          $sessions = [
            [
              'key' => 'username',
              'value' => $_POST['username'],
            ],
            [
              'key' => 'token',
              'value' => $this->Cache->getData('settings', '\\FileManager\\Settings', 'getSettings')['token'],
            ],
          ];
          $this->session->createSession($sessions);

          $this->return_array = [TRUE, $this->logs];
        }
      }
      $this->errors[] = 'User/password combination does not match any results.';
    } else {
      $this->errors[] = $this->password->isPasswordSecure($this->user['password'])[1];
    }

    if (!isset($this->return_array[1])) {
      if (count($this->errors) === 0) {
        $this->errors[] = 'Something unexpected went wrong.';
      }

      $this->return_array = [FALSE, $this->errors];
    }
  }

  /**
   * Returns necessary info
   * 
   * @return array
   */
  public function return() {
    return $this->return_array;
  }

}
