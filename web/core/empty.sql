--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `file_buttons`
--

CREATE TABLE `file_buttons` (
  `id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `value` varchar(2552) NOT NULL,
  `onclick` text NOT NULL,
  `class` varchar(255) NOT NULL,
  `js` text NOT NULL,
  `css` text NOT NULL,
  `permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `file_buttons`
--

INSERT INTO `file_buttons` (`id`, `sort`, `value`, `onclick`, `class`, `js`, `css`, `permissions`) VALUES
(0, 5, 'Compress', '', 'button_compress', '  $(\".button_compress\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Compress files into archive\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 320);\r\n    $(\".dialog__content\").load(\"./actions/files/compress.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Add content'),
(1, 3, 'Copy', '', 'button_copy', '  $(\".button_copy\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Copy files\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 320);\r\n    $(\".dialog__content\").load(\"./actions/files/copy.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Edit content'),
(2, 4, 'Move', '', 'button_move', '  $(\".button_move\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Move items\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 320);\r\n    $(\".dialog__content\").load(\"./actions/files/move.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Edit content'),
(3, 2, 'Rename', '', 'button_rename', '  $(\".button_rename\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Rename item\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 200);\r\n    $(\".dialog__content\").load(\"./actions/files/rename.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Edit content'),
(4, 6, 'Delete', '', 'button_delete', '  $(\".button_delete\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 320);\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Delete items\");\r\n    $(\".dialog__content\").load(\"./actions/files/delete.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Delete content'),
(5, 1, 'Edit', '', 'button_edit', '  $(\".button_edit\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Edititem\");\r\n    $(\"#dialog\").dialog(\"option\", \"width\", 640);\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 480);\r\n    $(\".dialog__content\").load(\"./actions/files/edit.php?path=\" + path + \"&files=\" + allFiles, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Edit content');

-- --------------------------------------------------------

--
-- Table structure for table `file_revisions`
--

CREATE TABLE `file_revisions` (
  `id` int(255) NOT NULL,
  `timestamp` int(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `revision_content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `folder_buttons`
--

CREATE TABLE `folder_buttons` (
  `id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `value` varchar(2552) NOT NULL,
  `onclick` text NOT NULL,
  `class` varchar(255) NOT NULL,
  `js` text NOT NULL,
  `css` text NOT NULL,
  `permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `folder_buttons`
--

INSERT INTO `folder_buttons` (`id`, `sort`, `value`, `onclick`, `class`, `js`, `css`, `permissions`) VALUES
(1, 1, 'New folder', '', 'button_folder_new_folder', '  $(\".button_folder_new_folder\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"New folder\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 200);\r\n    $(\".dialog__content\").load(\"./actions/folders/new.php?path=\" + path, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Add content'),
(2, 2, 'Rename', '', 'button_folder_rename', '  $(\".button_folder_rename\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Rename folder\");\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 200);\r\n    $(\".dialog__content\").load(\"./actions/folders/rename.php?path=\" + path, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Edit content'),
(3, 4, 'Delete', '', 'button_folder_delete', '  $(\".button_folder_delete\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 200);\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Delete folder\");\r\n    $(\".dialog__content\").load(\"./actions/folders/delete.php?path=\" + path, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Delete content'),
(5, 3, 'Upload', '', 'button_folder_upload', '  $(\".button_folder_upload\").on(\"click\", function() {\r\n    $(\"#dialog\").dialog(\"option\", \"height\", 320);\r\n    $(\"#dialog\").dialog(\"option\", \"title\", \"Upload folder\");\r\n    $(\".dialog__content\").load(\"./actions/folders/upload.php?path=\" + path, function() {\r\n      $(\"#dialog\").dialog(\"open\");\r\n    });\r\n  });', '', 'Add content');

-- --------------------------------------------------------

--
-- Table structure for table `folder_settings`
--

CREATE TABLE `folder_settings` (
  `id` int(1) NOT NULL,
  `path` varchar(255) NOT NULL,
  `view_mode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(12) NOT NULL,
  `timestamp` int(12) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `log` text NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `setting` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`setting`, `value`, `timestamp`) VALUES
('current_path', '', 0),
('files_cache', '', 0),
('last_index', '', 0),
('new_path', '', 0),
('settings', '', 0),
('status', 'Freshly installed!', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `unique_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `theme` varchar(20) NOT NULL DEFAULT 'dark',
  `permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`unique_id`, `username`, `password`, `email`, `theme`, `permissions`) VALUES
(0, 'Anonymous', '', '', 'dark', ''),
(1000000, '{USERNAME}', '{PASSWORD}', 'admin@website.com', 'dark', '[]');
-- --------------------------------------------------------

--
-- Table structure for table `view_modes`
--

CREATE TABLE `view_modes` (
  `id` int(11) NOT NULL,
  `view_mode` varchar(255) NOT NULL,
  `max_fn_length` int(11) NOT NULL,
  `prefix` text NOT NULL,
  `suffix` text NOT NULL,
  `code` text NOT NULL,
  `css` text NOT NULL,
  `js` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `view_modes`
--

INSERT INTO `view_modes` (`id`, `view_mode`, `max_fn_length`, `prefix`, `suffix`, `code`, `css`, `js`) VALUES
(1, 'icon', 0, '<div class=\'FilesOverview\'>', '</div>', '  <a href=\'{url}\' class=\'FileIconUrl\' target=\'_blank\' title=\'{full_filename}\'>\r\n    <div class=\'File\'>\r\n      <div class=\'ImageContainer\'>\r\n        <img class=\'Image\' src=\'{icon}\' />\r\n      </div>\r\n      <div class=\'Text FileName\' title=\'{full_filename}\'>{filename}</div>\r\n    </div>\r\n  </a>', '.File {\r\n  border-radius: 3px;\r\n  margin: 10px;\r\n  padding: 10px;\r\n  float:left;\r\n  width:64px;\r\n  height:80px;\r\n}\r\n\r\n.File:hover {\r\n  background-color:{accentColorHover};\r\n}\r\n\r\n.selected {\r\n  background-color:{accentColorSelected};\r\n}\r\n\r\n.File .ImageContainer {\r\n  display: flex;\r\n  height: 64px;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.File .Image {\r\n  max-height:64px;\r\n  max-width:64px;\r\n  text-align: center;\r\n}\r\n\r\n.File .Text {\r\n  overflow:hidden;\r\n  max-height:19px;\r\n  text-align:center;\r\n}', '$(\".FileIconUrl\").dblclick(function() {\r\n  window.open($(this).attr(\'href\'),\'_blank\');\r\n});\r\n$(\".FileIconUrl\").click(function(e) { \r\n  e.preventDefault();\r\n});'),
(2, 'image', 0, '<div class=\'FilesOverview\'>', '</div>', '  <a href=\'{url}\' class=\'FileIconUrl\' target=\'_blank\' title=\'{full_filename}\'>\r\n    <div class=\'File\'>\r\n      <div class=\'ImageContainer\'>\r\n        <img class=\'Thumbnail\' src=\'{thumbnail}\' />\r\n      </div>\r\n      <div class=\'Text FileName\' title=\'{full_filename}\'>{filename}</div>\r\n    </div>\r\n  </a>\r\n  <div class=\'dropdown-content\'>\r\n    <img class=\'Image\' src=\'{url}\' />\r\n    <div class=\'desc\'>\r\n      {full_filename}\r\n    </div>\r\n  </div>', '.File {\r\n  border-radius: 3px;\r\n  padding:10px;\r\n  margin: 10px;\r\n  float:left;\r\n  width:64px;\r\n  height:80px;\r\n}\r\n\r\n.File:hover {\r\n  background-color:{accentColorHover};\r\n}\r\n\r\n.selected {\r\n  background-color:{accentColorSelected};\r\n}\r\n\r\n.File .ImageContainer {\r\n  display: flex;\r\n  height: 64px;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.File .Thumbnail {\r\n  max-height:64px;\r\n  max-width:64px;\r\n  text-align: center;\r\n}\r\n\r\n.File .Text {\r\n  overflow:hidden;\r\n  max-height:19px;\r\n  text-align:center;\r\n}', '$(\".FileIconUrl\").dblclick(function() {\r\n  window.open($(this).attr(\'href\'),\'_blank\');\r\n});\r\n$(\".FileIconUrl\").click(function(e) { \r\n  e.preventDefault();\r\n  var selectedFile = $(this).attr(\'title\');\r\n});'),
(3, 'previews', 0, '<div class=\'FilesOverview\'>', '</div>', '  <div class=\'dropdown image-view-mode\'>\r\n    <a href=\'{url}\' class=\'FileIconUrl\' target=\'_blank\' title=\'{full_filename}\'>\r\n      <div class=\'File\'>\r\n        <div class=\'ImageContainer\'>\r\n          <img class=\'Thumbnail\' src=\'{thumbnail}\' />\r\n        </div>\r\n        <div class=\'Text FileName\' title=\'{full_filename}\'>{filename}</div>\r\n      </div>\r\n    </a>\r\n    <div class=\'dropdown-content\'>\r\n      <a href=\'{url}\' class=\'preview_link\' target=\'_blank\'>\r\n        <img class=\'Image\' src=\'{url}\' />\r\n      </a>\r\n      <div class=\'desc\'>\r\n        {full_filename}\r\n      </div>\r\n    </div>\r\n  </div>', '.File {\r\n  border-radius: 3px;\r\n  padding:10px;\r\n  margin: 10px;\r\n  float:left;\r\n  width:64px;\r\n  height:80px;\r\n}\r\n\r\n.File:hover {\r\n  background-color:{accentColorHover};\r\n}\r\n\r\n.selected {\r\n  background-color:{accentColorSelected};\r\n}\r\n\r\n.File .ImageContainer {\r\n  display: flex;\r\n  height: 64px;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.File .Thumbnail {\r\n  max-height:64px;\r\n  max-width:64px;\r\n  text-align: center;\r\n}\r\n\r\n.image-view-mode .dropdown-content {\r\n  margin-top: 120px;\r\n  margin-left: -50px;\r\n}\r\n\r\n.dropdown .Image {\r\n  max-height:480px;\r\n  max-width:480px;\r\n}\r\n\r\n.dropdown-content .desc {\r\n  max-width: 480px;\r\n  text-overflow: ellipsis;\r\n  white-space: nowrap;\r\n  border:1px solid #1f7f1c;\r\n}\r\n\r\n.image-view-mode {\r\n  text-align:center;\r\n}\r\n\r\n.File .Text {\r\n  overflow:hidden;\r\n  max-height:19px;\r\n  text-align:center;\r\n}', '$(\".FileIconUrl\").dblclick(function() {\r\n  window.open($(this).attr(\'href\'),\'_blank\');\r\n});\r\n$(\".FileIconUrl\").click(function(e) { \r\n  e.preventDefault();\r\n  var selectedFile = $(this).attr(\'title\');\r\n});'),
(4, 'list', 38, '<table class=\'FilesOverview\'>\r\n  <thead>\r\n    <tr class=\'FileHeading\'>\r\n      <th></th>\r\n      <th>Filename</th>\r\n      <th>Created</th>\r\n    <th>Filesize</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>', '  </tbody>\r\n</table>', '    <tr class=\'File\'>\r\n      <td class=\'ImageContainer\'><img class=\'Image\' src=\'{icon}\'></td>\r\n      <td class=\'Text\'><a class=\'FileName FileIconUrl\' title=\'{full_filename}\' href=\'{url}\'>{filename}</a></td>\r\n      <td class=\'Text FileCreated\'>{creation_time}</td>\r\n      <td class=\'Text FileSize\'>{filesize}</td>\r\n    </tr>', '.FilesOverview {\r\n  margin: 10px;\r\n  padding: 10px;\r\n  width:100%;\r\n  margin-left: -10px;\r\n}\r\n\r\n.FileHeading th {\r\n  border-bottom:1px dotted #FFF;\r\n}\r\n\r\n.File td {\r\n  cursor: pointer;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n}\r\n\r\n.File:hover {\r\n  background-color:{accentColorHover};\r\n}\r\n\r\n.selected {\r\n  background-color:{accentColorSelected};\r\n}\r\n\r\n.File .ImageContainer {\r\n  text-align:center;\r\n  width: 24px;\r\n}\r\n\r\n.File .Text {\r\n  padding-left: 10px;\r\n  padding-right: 10px;\r\n  overflow:hidden;\r\n  max-height:30px;\r\n  white-space: nowrap;\r\n}\r\n\r\n.File .FileSize {\r\n  text-align:center;\r\n  max-width: 70px;\r\n}\r\n\r\n.File .FileCreated {\r\n  text-align:center;\r\n}\r\n\r\n.File .Image {\r\n  max-height:24px;\r\n  max-width:24px;\r\n  margin-top: 2px;\r\n}', '$(\".FileIconUrl\").dblclick(function() {\r\n  window.open($(this).attr(\'href\'),\'_blank\');\r\n});\r\n$(\".FileIconUrl\").click(function(e) { \r\n  e.preventDefault();\r\n});');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_buttons`
--
ALTER TABLE `file_buttons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_revisions`
--
ALTER TABLE `file_revisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folder_buttons`
--
ALTER TABLE `folder_buttons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folder_settings`
--
ALTER TABLE `folder_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`setting`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`unique_id`);

--
-- Indexes for table `view_modes`
--
ALTER TABLE `view_modes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `file_buttons`
--
ALTER TABLE `file_buttons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `file_revisions`
--
ALTER TABLE `file_revisions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `folder_buttons`
--
ALTER TABLE `folder_buttons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `folder_settings`
--
ALTER TABLE `folder_settings`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `view_modes`
--
ALTER TABLE `view_modes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;