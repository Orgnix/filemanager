<?php

namespace UserFramework;

/**
 * Include necessary classes
 */
foreach (glob(__DIR__ . "/Components/*.php") as $filename) {
  if (is_file($filename)) {
    include_once($filename);
  }
}
/**
 * Include necessary permission instances
 */
foreach (glob(__DIR__ . "/Components/Permissions/*.php") as $filename) {
  if (is_file($filename)) {
    include_once($filename);
  }
}
