<?php

namespace UserFramework\Components;

/**
 * CreateUserId Class
 */
class CreateUserId {

  /**
   * @var string
   */
  protected $stringLength;

  /**
   * @var string
   */
  protected $method;

  /**
   * @var string
   */
  protected $customCharList;

  /**
   * CreateUserId Constructor.
   *
   * @param string $method
   *   Allowed values: numeric, varchar, alphabetic, custom
   */
  public function __construct($string_length = 7, $method = 'numeric', $custom_char_list = 'abcABC123-_+=') {
    $this->stringLength = $string_length !== NULL && !empty($string_length) ? $string_length : 7;
    $this->method = $method;
    $this->customCharList = $custom_char_list;
  }

  /**
   * Get available characters from inputted method
   *
   * @return string|bool
   */
  protected function getCharList() {
    if ($this->method === 'numeric') {
      return '0123456789';
    } elseif($this->method === 'alphabetic') {
      return '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } elseif($this->method === 'varchar') {
      return '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~`!@#$%^&*()_+=-[];,./?:}{';
    } elseif($this->method === 'custom') {
      return $this->customCharList;
    }
    return FALSE;
  }

  /**
   * Return a unique ID
   *
   * @return string|bool
   */
  public function UniqueID() {
    $characters = $this->getCharList();
    if (!empty($characters)) {
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $this->stringLength; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    return FALSE;
  }

  /**
   * Get the number of possible combinations.
   *
   * @return string
   */
  public function getPossibleCombinations() {
    $characters = $this->getCharList();

    $char_length = strlen($characters);
    for ($i=1;$i<$this->stringLength;$i++) {
      $char_length = $char_length * strlen($characters);
    }
    
    return $char_length;
  }

}
