<?php

namespace UserFramework\Components;

/**
 * Base for permissions
 */
interface PermissionsInterface {

  /**
   * Returns true or false based on the condition
   * 
   * @return bool
   */
  public function condition();

  /**
   * Returns the label of the permission
   * 
   * @return string
   */
  public function label();

}
