<?php

namespace UserFramework\Components;

use FileManager\Core;
use FileManager\Components\Url;
use UserFramework\Components\User;

/**
 * Permissions class
 */
class Permissions {
  
  /** @var \FileManager\Core $ore */
  protected $core;
  
  /**
   * Permissions constructor
   */
  public function __construct() {
    $this->core = new Core();
  }

  /**
   * Checks if user has permission based given permission type
   *
   * @param string $permission
   * @param array $values
   * @param bool $log
   *
   * @return bool
   */
  public function hasPermission($permission, $values = [], $log = false) {
    $class = 'UserFramework\Components\Permissions\\' . $permission;
    $perm = new $class($values);
    if ($perm->condition() === FALSE && $log) {
      $this->core->log(User::getUsername() . ' tried accessing ' . Url::getCurrentPath());
    }
    return $perm->condition();
  }

}
