<?php

namespace UserFramework\Components;

class UserExists {

  /** @var \FileManager\Components\Database $database */
  protected $database;

  /** @var array $conditions condition on which to check for in database */
  protected $conditions;

  /** @var string $id the database field name of the user id */
  protected $id;

  /** @var string $table the database user table */
  protected $table;
	
  /**
   * UserExists Constructor.
   */
  public function __construct($database, $conditions, $id, $table) {
    $this->database = $database;
    $this->conditions = $conditions;
    $this->id = $id;
    $this->table = $table;
  }

  /**
   * Check if user exists, on condition given
   *
   * @return mixed
   */
  public function CheckIfExists() {
    $users = $this->database;

    $users->select($this->table, 'u');
    foreach ($this->conditions as $field => $value) {
      $users->condition($field, $value);
    }
    $users->fields('u', [$this->id, 'password']);
    $users->execute();

    $result = $users->fetchAllAssoc();

    // If the query returned more than 0 results, return TRUE
    if (count($result) > 0) {
      return [TRUE, $result[0]];
    }

    // If the query returned 0 results, return FALSE
    return FALSE;
  }

}
