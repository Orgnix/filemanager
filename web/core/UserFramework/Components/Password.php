<?php

namespace UserFramework\Components;

/**
 * Password class
 *
 * default : password_hash
 */
class Password {

  /**
   * @var array
   */
  protected $difficulty;

  /**
   * Password constructor
   *
   * @param array $difficulty
   */
  public function __construct(array $difficulty = ['aA0-', 8]) {
    $this->difficulty = $difficulty;
  }

  /**
   * Return the difficulty
   * 
   * @return array
   */
  public function getDifficulty() {
    return $this->difficulty;
  }

  /**
   * Explains why the password wasn't valid
   * 
   * @return string
   */
  public function getDifficultyExplanation() {
    $difficulty = $this->getDifficulty();
    if ($difficulty[0] === 'a0') {
      return '[Password]: at least 8 characters, lowercase letters and numbers required.';
    } elseif ($difficulty[0] === 'aA0') {
      return '[Password]: at least 8 characters, lowercase letters, uppercase letters and numbers required.';
    } elseif ($difficulty[0] === 'aA0-') {
      return '[Password]: at least 8 characters, lowercase letters, uppercase letters, special characters and numbers required.';
    } else {
      return '[Password]: no valid difficulty defined. Valid ones: a0 | aA0 | aA0-';
    }
  }

  /**
   * Check if password is secure
   * 
   * @param string $password
   * 
   * @return array(boolean,string)
   */
  public function isPasswordSecure($password) {
    $difficulty = $this->getDifficulty();
    if (strlen($password) < $difficulty[1]) {
      return [FALSE, '[Password]: Too short (min ' . $difficulty[1] . ' characters).'];
    } elseif ($difficulty[0] === 'a0') {
      $hasnumber = FALSE;
      $hasletter = FALSE;
      for ($i = 0; $i <= strlen($password); $i++) {
        $char = substr($password, $i, 1);
        if (is_numeric($char)) {
          $hasnumber = TRUE;
        } elseif (ctype_alpha($char)) {
          $hasletter = TRUE;
        }
      }
      if ($hasnumber && $hasletter) {
        return [TRUE];
      }
    } elseif($difficulty[0] === 'aA0') {
      $hasnumber = FALSE;
      $hasletter = FALSE;
      $hasuppercase = FALSE;
      for ($i = 0; $i <= strlen($password); $i++) {
        $char = substr($password, $i, 1);
        if (is_numeric($char)) {
          $hasnumber = TRUE;
        } elseif (ctype_alpha($char)) {
          if (preg_match('~^\p{Lu}~u', $char)) {
            $hasuppercase = TRUE;
          } else {
            $hasletter = TRUE;
          }
        }
      }
      if ($hasnumber && $hasletter && $hasuppercase) {
        return [TRUE];
      }
    } elseif($difficulty[0] === 'aA0-') {
      $hasnumber = FALSE;
      $hasletter = FALSE;
      $hasuppercase = FALSE;
      $hasspecial = FALSE;
      for ($i = 0; $i <= strlen($password); $i++) {
        $char = substr($password, $i, 1);
        if (is_numeric($char)) {
          $hasnumber = TRUE;
        } elseif (ctype_alpha($char)) {
          if (preg_match('~^\p{Lu}~u', $char)) {
            $hasuppercase = TRUE;
          } else {
            $hasletter = TRUE;
          }
        } elseif(preg_match('/[\'^£$%&*()}{@#~?!><>,|=_+¬-]/', $char)) {
          $hasspecial = TRUE;
        }
      }
      if ($hasnumber && $hasletter && $hasuppercase && $hasspecial) {
        return [TRUE];
      }
    }
    return [FALSE, '[Password]: Does not meet the difficulty requirements! <br />' . $this->getDifficultyExplanation()];
  }

  /**
   * Return the password in a secured manner
   * 
   * @return array(boolean, string)
   */
  public function securePassword($password) {
    if ($this->isPasswordSecure($password)[0]) {
      return [TRUE, password_hash($password, PASSWORD_DEFAULT)];
    }
    return $this->isPasswordSecure($password);
  }

  /**
   * Checks password that was entered against the hash
   * 
   * @param  string $password
   * @param  string $hash
   * 
   * @return array(boolean, string)
   */
  public function checkPassword($password, $hash) {
    if (!empty($password) && !empty($hash)) {
      return [TRUE, password_verify($password, $hash)];
    }
    return [FALSE, '[Password]: Please enter parameters'];
  }

}
