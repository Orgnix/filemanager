<?php

namespace UserFramework\Components;

use FileManager\Components\Database;
use FileManager\Settings;
use FileManager\Core;

/**
 * CreateUserId Class
 */
class User extends Settings {

  /** @var string $username */
  protected $username;

  /** @var \FileManager\Core $core */
  protected $core;

  /**
   * User constructor
   *
   * @param string $username
   */
  public function __construct($username) {
    parent::__construct();
    $this->username = $username;
    $this->core = new Core();
    $this->connect();
  }

  /**
   * Returns the user's displayable name
   * 
   * @return string
   */
  public static function getUsername() {
    if (isset($_SESSION['username'])) {
      return $_SESSION['username'];
    }
    return 'Anonymous';
  }

  /**
   * Returns a certain user value
   *
   * @param string $setting
   *
   * @return string
   */
  public function getValue($setting) {
    if ($this->Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])[$setting] !== NULL) {
      return $this->Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])[$setting];
    } else {
      $query = $this->database;

      $query->select('users')
        ->condition('username', $this->username)
        ->fields(NULL, [$setting])
        ->execute();

      if ($result = $query->fetchAllAssoc()) {
        return $result[0][$setting];
      }
    }

    return FALSE;
  }

  /**
   * Returns all current user's values
   *
   * @return string
   */
  public function getValues() {
    $query = $this->database;

    $query->select('users')
      ->condition('username', $this->username)
      ->fields(NULL)
      ->execute();

    if ($result = $query->fetchAllAssoc()) {
      return $result[0];
    }

    return FALSE;
  }

  /**
   * Sets a certain user's given key to given value
   * 
   * @param string $setting
   * @param string $value
   *
   * @return bool
   */
  public function setValue($setting, $value) {
    $query = $this->database;

    $query->update('users')
          ->condition('username', $this->username)
          ->values([$setting => $value]);
    if ($query->execute()) {
      $this->core->log('User <i>' . self::getUsername() . '</i> saved');
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Saves multiple user values in one query
   *
   * @param string $id
   * @param array $values
   *
   * @return bool
   */
  public function saveUserValues($id, $values) {
    $query = $this->database;

    $query->update('users')
          ->values($values)
          ->condition('unique_id', $id);
    if ($query->execute()) {
      $this->core->log('User <i>' .$this->getUser($id)['username'] . '</i> saved');
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get given user from database
   *
   * @param string|int $user_id
   *
   * @return mixed
   */
  public function getUser($user_id) {
    $query = $this->database;

    $query->select('users')
          ->fields(NULL, ['unique_id', 'username', 'email', 'theme', 'permissions'])
          ->condition('unique_id', $user_id)
          ->execute();

    if ($result = $query->fetchAllAssoc()) {
      return $result[0];
    }

    return FALSE;
  }

  /**
   * Gets all users from database
   *
   * @return mixed
   */
  public function getAllUsers() {
    $query = $this->database;

    $query->select('users')
          ->fields(NULL, ['unique_id', 'username', 'email', 'theme', 'permissions'])
          ->orderBy('username', 'ASC')
          ->execute();

    if ($result = $query->fetchAllAssoc()) {
      return $result;
    }

    return FALSE;
  }
  
  /**
   * Removes user
   */
  public function remove($id = NULL) {
    if ($id != 1000000 && $this->username != $this->getUser(1000000)['username']) {
      $query = $this->database;
      $query->delete('users');
      if ($id == NULL) {
        $query->condition('username', $this->username);
      } else {
        $query->condition('unique_id', $id);
      }
      if ($query->execute()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Connect to the database
   * 
   * @return bool
   */
  public function connect() {
    if (!$this->database) {
      $this->database = new Database('OR');

      return TRUE;
    }

    return FALSE;
  }

}
