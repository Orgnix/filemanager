<?php

namespace UserFramework\Components;

use FileManager\Core;

class Session {

  /** @var string */
  protected $createCookies;

  /** @var \FileManager\Core $core */
  protected $core;
	
  /**
   * Session constructor.
   *
   * @param bool $create_cookies
   */
  public function __construct($create_cookies = FALSE) {
    $this->createCookies = $create_cookies;
    $this->core = new Core();
  }

  /**
   * Returns boolean to know whether we're allowed to create cookies
   * 
   * @return bool
   */
  public function allowedToCreateCookies() {
    return $this->createCookies;
  }

  /**
   * Create session
   *
   * @param array $sessions
   * 
   * @return array(boolean, string)
   */
  public function createSession(array $sessions) {
    if(is_array($sessions)) {
      foreach ($sessions as $session) {
        if ($session['key'] !== 'password') {
          $_SESSION[$session['key']] = $session['value'];
        }
      }
      if ($this->allowedToCreateCookies()) {
        $cookies = [];
        foreach ($sessions as $session) {
          if ($session['key'] !== 'password') {
            $cookies[] = [
              'key' => $session['key'],
              'value' => $session['value'],
            ];
          }
        }
        $this->createCookie($cookies);
      }
      $this->core->log('Session opened for ' . User::getUsername());
      return [TRUE, 'Session(s) created'];
    }
    return [FALSE, 'Parameter is not a multidimensional array'];
  }

  /**
   * Create cookie
   *
   * @param array $cookies
   * 
   * @return array(boolean, string)
   */
  public function createCookie(array $cookies) {
    if(is_array($cookies)) {
      foreach ($cookies as $cookie) {
        if (is_array($cookie)) {
          $time = isset($cookie['time']) ? $cookie['time'] : time()+(3600*24*30); // Standard 1 month
          setcookie($cookie['key'], $cookie['value'], $time);
        }
      }
      return [TRUE, 'Cookie(s) created'];
    }
    return [FALSE, 'Parameter is not a multidimensional array'];
  }

  /**
   * Create session from cookie
   *
   * @param int $sessionkey
   *
   * @return bool
   */
  public static function createSessionFromCookies() {
    if (isset($_COOKIE)) {
      foreach ($_COOKIE as $cookie_key => $cookie_val) {
        $_SESSION[$cookie_key] = $cookie_val;
      }
      return TRUE;
    }
    return FALSE;
  }

}
