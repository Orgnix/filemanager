<?php

namespace UserFramework\Components\Permissions;

use UserFramework\Components\PermissionsInterface;
use UserFramework\Components\User;

/**
 * Checks if user is logged in
 */
class Custom implements PermissionsInterface {

  /** @var \UserFramework\Components\User $user */
  protected $user;

  /** @var string $permission */
  protected $permission;

  /**
   * Role constructor.
   *
   * @param array $values
   */
  public function __construct($values = []) {
    $this->permission = $values['permission'];
    $this->user = new User(!is_null($values['user']) ? $values['user'] : User::getUsername());
  }

  /**
   * {@inheritdoc}
   */
  public function condition() {
    $user_permissions = json_decode($this->user->getValue('permissions'));
    if (!is_array($user_permissions)) { $user_permissions = []; }
    if (in_array($this->permission, $user_permissions)
      || $this->user->getValue('unique_id') == 1000000) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return 'Custom';
  }

}
