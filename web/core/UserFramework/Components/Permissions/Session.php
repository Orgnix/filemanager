<?php

namespace UserFramework\Components\Permissions;

use UserFramework\Components\PermissionsInterface;
use UserFramework\Components\Session as SessionClass;

/**
 * Checks if user is logged in
 */
class Session implements PermissionsInterface {

  /** @var \UserFramework\Components\Session $session */
  protected $session;

  /**
   * Session constructor.
   *
   * @param array $values
   */
  public function __construct($values) {
    $this->session = new SessionClass(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function condition() {
    if (isset($_SESSION['username'])) {
      return TRUE;
    } elseif (isset($_COOKIE['username'])) {
      $this->session->createSessionFromCookies();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return 'Session';
  }

}
