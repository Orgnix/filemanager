<?php

namespace FileManager;

/**
 * Make use of all the necessary classes
 */
use UserFramework\Components\CreateUserId;
use UserFramework\Components\UserExists;
use UserFramework\Components\Password;
use UserFramework\Components\Session;
use FileManager\Settings;

/**
 * CreateUser class
 */
class CreateUser extends Settings {

  /** @var array $user */
  protected $user;

  /** @var array $logs */
  protected $logs;

  /** @var array $errors */
  protected $errors;

  /** @var array $return_array */
  protected $return_array;

  /** @var \UserFramework\Components\Session $session */
  protected $session;

  /** @var \UserFramework\Components\Password $password */
  protected $password;

  /** @var \UserFramework\Components\CreateUserId $createuserid */
  protected $createUserId;

  /** @var \UserFramework\Components\UserExists $userExists */
  protected $userExists;

  /**
   * CreateUser constructor
   * 
   * @param string $username
   * @param string $password
   * @param string $email
   */
  public function __construct($username, $password, $email) {
    parent::__construct();
    $this->user['username'] = $username;
    $this->user['password'] = $password;
    $this->user['email'] = $email;
    $this->errors = [];
    $this->logs = [];
    $this->return_array = [];

    $this->session = new Session();
    $this->createUserId = new CreateUserId();
    $this->password = new Password();

    $this->connect();

    $this->userExists = new UserExists($this->database, ['username' => $this->user['username'], 'email' => $this->user['email']], 'unique_id', 'users');
    
    if ($this->password->securePassword($this->user['password'])[0]) {
      if (!$this->userExists->CheckIfExists()[0]) {
        $values = [
          'unique_id' => $this->createUserId->UniqueID(),
          'username' => $this->user['username'],
          'password' => $this->password->securePassword($this->user['password'])[1],
          'email' => $this->user['email'],
          'theme' => 'dark',
          'permissions' => '[]',
        ];

        $query = $this->database;
        $query->insert('users', 'u')
              ->values($values);
        if ($query->execute()) {
          $this->logs[] = 'Added user ' . $this->user['username'] . ' successfully.';
          $this->logs[] = 'Permissions and user theme can be changed on changing the user.';
          $this->return_array = [TRUE, $this->logs];
        }
      }
      $this->errors[] = '[User]: User already exists either by username or email.';
    } else {
      $this->errors[] = $this->password->isPasswordSecure($this->user['password'])[1];
    }

    if (!isset($this->return_array[1])) {
      if (count($this->errors) === 0) {
        $this->errors[] = 'Something unexpected went wrong.';
      }

      $this->return_array = [FALSE, $this->errors];
    }
  }

  /**
   * Returns necessary info
   * 
   * @return array
   */
  public function return() {
    return $this->return_array;
  }

}
