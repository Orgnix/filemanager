<?php

/**
 * Includes
 */
include_once(__DIR__ . '/../vendor/autoload.php');
include_once(__DIR__ . '/settings.php');
include_once(__DIR__ . '/core/includes.php');

use FileManager\Components\Filesystem;
use FileManager\Components\Theme;
use UserFramework\Components\User;
use UserFramework\Components\Permissions;
use FileManager\Settings;
use FileManager\Core;
use FileManager\Components\ViewMode;
use FileManager\Components\Buttons;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

if ($settings['local_only']) {
  if (!in_array($_SERVER['REMOTE_ADDR'], $settings['local_only_remote_addr'])) {
    $loader = new FilesystemLoader('themes/dark/');
    $twig = new Environment($loader);
    $template = $twig->load('not_allowed.html.twig');
    echo $template->render();
    die;
  }
}

/**
 * Check for permissions
 */
$permissions = new Permissions();
if (!$Cache->getData('permissions_custom_administer_site', '\\UserFramework\\Components\\Permissions', 'hasPermission', ['Custom', ['user' => NULL, 'permission' => 'Administer site']])) {
    header('Location: ./login.php');
  die;
}

$Core = new Core();
$ViewMode = new ViewMode();
$Buttons = new Buttons();
$User = new User(User::getUsername());
define('USER_THEME', $Cache->getData('user', '\\UserFramework\\Components\\User', 'getValues', [], [User::getUsername()])['theme']);
$loader = new FilesystemLoader('themes/' . USER_THEME . '/');
$twig = new Environment($loader);
$Settings = new Settings();
$Theme = new Theme(USER_THEME);

/* START CRON */
$status = $Core->cron();
/* STOP CRON */

$all_view_modes = $ViewMode->getAllViewModes();

if (isset($_GET['view_mode'])) {
  $view_mode = $ViewMode->getViewMode($_GET['view_mode'])[0];
  if (isset($_POST['view_mode_save'])) {
    $values = [];
    foreach ($_POST as $key => $value) {
      $key = str_replace('view_mode_', '', $key);
      if ($key != 'action' && $key != 'id' && $key != 'save') {
        $values[$key] = $value;
      }
    }
    $Core->log('Saved view mode [' . $values['view_mode'] . '].');
    $ViewMode->save($values['view_mode'], $values);
    header('Refresh:0;');
  }
}
if (isset($_POST['view_mode_add'])) {
  $values = ['id' => NULL];
  foreach ($_POST as $key => $value) {
    $key = str_replace('view_mode_', '', $key);
    if ($key != 'action' && $key != 'add') {
      $values[$key] = $value;
    }
  }
  $Core->log('Added view mode <i>' . $values['view_mode'] . '</i>.');
  $ViewMode->add($values);
  header('Refresh:0;');
}

if (isset($_GET['remove']) && $_GET['remove'] == 'consent') {
  $Core->log('Removed view mode <i>' . $_GET['view_mode'] . '</i>');
  $ViewMode->remove($_GET['view_mode']);
  header('Location: viewModes.php');
}

$template = $twig->load('view_modes.html.twig');
echo $template->render([
  'debug' => $settings['debugging'],
  'view_mode' => isset($view_mode) ? $view_mode : NULL,
  'all_view_modes' => $all_view_modes,
  'assets' => $settings['assets'],
  'settings' => $settings,
  'remove' => isset($_GET['remove']),
  'status_bar' => [
    'status' => $status,
    'current_theme' => USER_THEME,
    'themes' => $Theme->getThemes(),
    'info' => [
      'version' => Core::FILEMANAGER_VERSION,
      'disk_space' => [
        'used_percentage' => round(100 / Filesystem::getTotalDiskSpace() * Filesystem::getUsedDiskSpace()),
        'formatted_used' => Filesystem::getUsedDiskSpace(TRUE),
        'formatted_total' => Filesystem::getTotalDiskSpace(TRUE),
      ],
    ],
  ],
]);
